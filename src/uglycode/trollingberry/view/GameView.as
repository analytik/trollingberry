package uglycode.trollingberry.view
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	import org.osflash.signals.DeluxeSignal;
	import org.osflash.signals.natives.NativeSignal;
	
	import qnx.fuse.ui.actionbar.ActionBar;
	import qnx.fuse.ui.buttons.Button;
	import qnx.fuse.ui.buttons.IconLabelButton;
	import qnx.fuse.ui.buttons.LabelButton;
	import qnx.fuse.ui.buttons.LabelPlacement;
	import qnx.fuse.ui.core.Container;
	import qnx.fuse.ui.core.TabAction;
	import qnx.fuse.ui.display.Image;
	import qnx.fuse.ui.layouts.Align;
	import qnx.fuse.ui.layouts.gridLayout.GridData;
	import qnx.fuse.ui.layouts.gridLayout.GridLayout;
	import qnx.fuse.ui.layouts.gridLayout.GridRowData;
	import qnx.fuse.ui.layouts.rowLayout.RowData;
	import qnx.fuse.ui.layouts.rowLayout.RowLayout;
	import qnx.fuse.ui.listClasses.ListSelectionMode;
	import qnx.fuse.ui.listClasses.ScrollDirection;
	import qnx.fuse.ui.listClasses.SectionList;
	import qnx.fuse.ui.skins.SkinStates;
	import qnx.fuse.ui.text.Label;
	import qnx.fuse.ui.text.TextFormat;
	import qnx.invoke.InvokeAction;
	import qnx.invoke.InvokeManager;
	import qnx.invoke.InvokeRequest;
	import qnx.ui.data.DataProvider;
	import qnx.ui.data.SectionDataProvider;
	
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.TrollingBerry;
	import uglycode.trollingberry.util.PoolThingy;
	import uglycode.trollingberry.view.ui.Utils;
	import uglycode.trollingberry.world.actions.Action;
	import uglycode.trollingberry.world.activities.Activity;
	
	public class GameView extends Container
	{
		private var topContainer:Container;
		private var mainContainer:Container;
		private var myRow:RowLayout;
		private var myRowD:RowData;
		private var ab:ActionBar;
		private var activityContainer:Container;
		private var actGridData:GridData;
		private var gridLayout:GridLayout;
		private var activities:Vector.<Activity>;
		
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		public function GameView()
		{
			//trace('gameview constructor');
			super();
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		override protected function init():void {
			//trace('in game view init');
			super.init();
			this.addEventListener(Event.ADDED_TO_STAGE, init2);
			ServiceLocator.getInstance().player.currentActivity = null;
		}
		
		
		
		public function init2(e:Event):void {
			//trace('game view added to stage');
			this.removeEventListener(Event.ADDED_TO_STAGE, init2);
			
			activityContainer = Utils.createContainer(this.stage, 'main', false);
			activityContainer.vScrollVisible = true;
			activityContainer.scrollDirection = ScrollDirection.VERTICAL;
			
			gridLayout = new GridLayout();
			gridLayout.horizontalPadding = 16;
			gridLayout.verticalPadding = 32;
			gridLayout.numColumns = 3;
			activityContainer.layout = gridLayout;
			
			var topTitleLayoutData:GridData = new GridData();
			topTitleLayoutData.preferredWidth = activityContainer.width - gridLayout.paddingLeft - gridLayout.paddingRight;
			topTitleLayoutData.hAlign = Align.CENTER;
			topTitleLayoutData.hSpan = 3;
			topTitleLayoutData.marginBottom = 24;
			
			/**
			 * Top label
			 */
			var lbl:Label = new Label();
			lbl.name = "Title";
			var format : TextFormat = PoolThingy.getTextFormat('default');
			lbl.format = format;
			lbl.text = 'Choose your activity';
			lbl.layoutData = topTitleLayoutData;
			
			activityContainer.addChild(lbl);
			addChild(activityContainer);
			
			actGridData = new GridData();
			actGridData.marginRight = actGridData.marginLeft = actGridData.marginTop = actGridData.marginBottom = 12;
			actGridData.preferredHeight = 128 + 12 + 12 + 30 + 12; //icon, margin top,bottom, font, font margin top
			actGridData.preferredWidth = (activityContainer.width - gridLayout.paddingLeft - gridLayout.paddingRight) / 3;
			actGridData.hAlign = Align.CENTER;
			var lab:Label;
			var img:Image;
			var miniContainer:Container;
			var imgGridData:GridData;
			var lblGridData:GridData;
			//var activities:Array = [new Activity, new Activity, new Activity, new Activity, new Activity, new Activity];
			activities = ServiceLocator.getInstance().player.getVisibleActivities();
			for each (var a:Activity in activities)
			{
				/*
				fuck you buttons
				var actButt:IconLabelButton = new IconLabelButton();
				actButt.setIcon(a.icon);
				actButt.label = 'World of ClickCraft';
				actButt.width = actGridData.preferredWidth; //ignored
				//actButt.height = 128 + 12 + 12 + 30 + 12;
				actButt.labelPlacement = LabelPlacement.BOTTOM;
				actButt.setTextFormatForState(PoolThingy.getTextFormat('verysmall'), SkinStates.UP);
				actButt.setTextFormatForState(PoolThingy.getTextFormat('verysmall'), SkinStates.DOWN);
				*/
				
				miniContainer = new Container();
				miniContainer.layout = new GridLayout(1);
				miniContainer.layoutData = actGridData;
				//ugly hack
				miniContainer.name = a.name;
				
				imgGridData = new GridData();
				imgGridData.marginRight = imgGridData.marginLeft = 12;
				imgGridData.marginTop = imgGridData.marginBottom = 12;
				imgGridData.hAlign = Align.CENTER;
				imgGridData.vAlign = Align.CENTER;
				
				img = new Image();
				//img.setImage(new BitmapData(128, 128, false, 0xff0000));
				img.setImage(a.icon);
				img.layoutData = imgGridData;
				miniContainer.addChild(img);
				
				lab = new Label();
				lab.text = a.name;
				lab.format = PoolThingy.getTextFormat('verysmall');
				lab.layoutData = imgGridData;
				miniContainer.addChild(lab);
				//miniContainer.addEventListener(MouseEvent.CLICK, activityClicked);
				var signalActivityClick:NativeSignal = new NativeSignal(miniContainer, MouseEvent.CLICK, MouseEvent);
				signalActivityClick.add(activityClicked);
				
				activityContainer.addChild(miniContainer);
			}
			
			var butt:LabelButton = new LabelButton();
			butt.label = 'Some spam (browser link)';
			butt.addEventListener(MouseEvent.CLICK, openSpamLink);
			butt.layoutData = topTitleLayoutData;
			activityContainer.addChild(butt);
		}
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		protected function activityClicked(event:MouseEvent):void
		{
			//var aa:Vector.<Action>;
			for each (var a:Activity in activities)
			{
				if (a.name == (event.currentTarget as Container).name) {
					//trace ('you touched my activity ' + a);
					//aa = a.getAvailableActions();
					ServiceLocator.getInstance().player.currentActivity = a;
					TrollingBerry.getInstance().switchStateTo('activity');
					break;
				}
			}
		}
		
		
		protected function openSpamLink(event:MouseEvent):void
		{
			//links to related articles - experiment
			var request:InvokeRequest = new InvokeRequest();
			//request.target = "com.example.image.view";
			request.action = InvokeAction.OPEN;
			request.mimeType = "text/html";
			request.uri = "http://goo.gl/eorDg";
			InvokeManager.invokeManager.invoke( request );
		}
		

		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	
		
		
		
		
		
	}
}

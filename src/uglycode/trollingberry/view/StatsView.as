package uglycode.trollingberry.view
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.Dictionary;
	import flash.utils.describeType;
	
	import qnx.fuse.ui.actionbar.ActionBar;
	import qnx.fuse.ui.buttons.CheckBox;
	import qnx.fuse.ui.buttons.LabelButton;
	import qnx.fuse.ui.core.Action;
	import qnx.fuse.ui.core.Container;
	import qnx.fuse.ui.core.SizeOptions;
	import qnx.fuse.ui.events.ActionEvent;
	import qnx.fuse.ui.layouts.Align;
	import qnx.fuse.ui.layouts.gridLayout.GridData;
	import qnx.fuse.ui.layouts.gridLayout.GridLayout;
	import qnx.fuse.ui.layouts.gridLayout.GridRowData;
	import qnx.fuse.ui.layouts.rowLayout.RowData;
	import qnx.fuse.ui.layouts.rowLayout.RowLayout;
	import qnx.fuse.ui.listClasses.ListSelectionMode;
	import qnx.fuse.ui.listClasses.ScrollDirection;
	import qnx.fuse.ui.listClasses.SectionList;
	import qnx.fuse.ui.progress.ProgressBar;
	import qnx.fuse.ui.text.Label;
	import qnx.fuse.ui.text.TextAlign;
	import qnx.fuse.ui.text.TextFormat;
	import qnx.ui.data.DataProvider;
	import qnx.ui.data.SectionDataProvider;
	
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.TrollingBerry;
	import uglycode.trollingberry.game.IStatProvider;
	import uglycode.trollingberry.util.PoolThingy;
	import uglycode.trollingberry.util.VerbalDescription;
	import uglycode.trollingberry.view.ui.Utils;
	
	public class StatsView extends Container
	{
		private var player:Player;
		private var _controlContainer : Container;
		[Deprecated]
		private var barVector:Vector.<ProgressBar>;
		private var labelVector:Vector.<Label>;
		private var containerData:RowData;
		
		
		
		private var topContainer:Container;
		private var mainContainer:Container;
		private var myRow:RowLayout;
		private var myRowD:RowData;
		private var centeredGridData:Object;
		private var labelGridData:GridData;
		private var progressGridData:GridData;
		

		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		public function StatsView()
		{
			super();
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		override protected function init():void {
			super.init();
			trace('StatsView.init()');
			this.player = ServiceLocator.getInstance().getPlayer();
			addEventListener(Event.ADDED_TO_STAGE, handleAddedToStage);
		}
		
		
		
		protected function init2():void {
			var ab:ActionBar = PoolThingy.getEmptyActionBar();
			ab.backButton = new Action('Back');
			ab.addEventListener(qnx.fuse.ui.events.ActionEvent.ACTION_SELECTED, goBackToGame);
			this.addChild(ab);
			
			_controlContainer = Utils.createContainer(this.stage, 'main', true);
			_controlContainer.vScrollVisible = true;
			_controlContainer.scrollDirection = ScrollDirection.VERTICAL;
			
			centeredGridData = new GridData();
			centeredGridData.marginTop = 12;
			centeredGridData.hAlign = Align.CENTER;
			labelGridData = new GridData();
			labelGridData.marginTop = 12;
			labelGridData.hAlign = Align.BEGIN;
			progressGridData = new GridData();
			progressGridData.marginTop = 6;
			progressGridData.marginBottom = 12;
			progressGridData.hAlign = Align.BEGIN;
			
			
			/*
			var allData:Vector.<IStatProvider> = new Vector.<IStatProvider>;
			var xml:XML = describeType(player);
			for each (var o:Object in xml..variable) {
			var propName:String = o.@name;
			//var type:String = o.@type;
			//trace('encountered one ' + name + ' - ' + type);
			//trace (' --- ' + player[propName]);
			if (player[propName] is IStatProvider) {
			trace('found IStatProvider property ' + propName);
			allData.push(player[propName]);
			}
			}
			*/
			
			//labelVector = new Vector.<Label>;
			//barVector = new Vector.<ProgressBar>;
			
			var key:String;
			var lbl:Label;
			var format : TextFormat = new TextFormat();
			var skillsDic:Dictionary;
			var allData:Vector.<IStatProvider> = player.provideStats();
			
			for (var index:int = 0; index < allData.length; index++)
			{
				var smallContainer:Container = Utils.createContainer(this.stage, 'small');
				smallContainer.width = 600;
				_controlContainer.addChild(smallContainer);
				skillsDic = allData[index].getStats();
				
				lbl = new Label();
				lbl.name = "title_"+index;				
				lbl.format = PoolThingy.getTextFormat();
				//lbl.text = allData[index].getStatTitle();
				lbl.htmlText = '<b><u>' + allData[index].getStatTitle() + '</u></b>';
				lbl.layoutData = centeredGridData; 
				//(lbl.layoutData as GridData).preferredWidth = 720-48;
				//lbl.minHeight = 64;
				smallContainer.addChild(lbl);
				
				for (key in skillsDic)
				{
					lbl = new Label();
					lbl.name = "lbl_"+key;				
					lbl.format = PoolThingy.getTextFormat();
					if (allData[index].getVerbalType() == 'skill') {
						lbl.text = key + ' (' + VerbalDescription.ofSkill(skillsDic[key].value, false) + ')';
					}
					else {
						lbl.text = key;
					}
					
					lbl.layoutData = labelGridData;
					//lbl.minHeight = 64;
					smallContainer.addChild(lbl);
					
					var myProgress:ProgressBar = new ProgressBar();
					//myProgress.setPosition(0, 0);
					//myProgress.height = 24;
					//myProgress.width = 250;
					myProgress.progress = ((skillsDic[key].value + 20) / 40);
					//11myProgress.setPosition(400, 24 + (60 * barVector.length));
					
					myProgress.layoutData = progressGridData;
					//barVector.push(myProgress);
					smallContainer.addChild(myProgress);
				}
			}
			
			
			addChild( _controlContainer );
		}
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		protected function goBackToGame(event:ActionEvent):void
		{
			//trace('action bar clicked');
			TrollingBerry.getInstance().switchStateTo('game');
		}
		
		
		
		private function handleCheckboxClick(e:Event):void {
			trace('button pressed');
		}
		
		
		
		private function handleAddedToStage(e:Event):void
		{
			removeEventListener(Event.ADDED_TO_STAGE,handleAddedToStage);
			// stage is avail, we can now listen for events
			//stage.addEventListener( Event.RESIZE, onResize );
			// force a resize call
			//onResize(new Event(Event.RESIZE));
			//initializeUI();
			init2();
		}
		

		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	

		

		
	}
}
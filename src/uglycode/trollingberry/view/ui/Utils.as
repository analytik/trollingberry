package uglycode.trollingberry.view.ui
{
	import flash.display.Sprite;
	import flash.display.Stage;
	
	import qnx.fuse.ui.core.Container;
	import qnx.fuse.ui.layouts.Align;
	import qnx.fuse.ui.layouts.gridLayout.GridData;
	import qnx.fuse.ui.layouts.gridLayout.GridLayout;
	import qnx.fuse.ui.layouts.rowLayout.RowLayout;
	import qnx.fuse.ui.listClasses.ScrollDirection;

	public class Utils
	{
		public function Utils()
		{
		}
		
		
		/**
		 * TODO create ActionBar
		 */
		
		
		
		/**
		 * todo move to Utils or a separate view.ui
		 */
		public static function createContainer(stage:Stage, type:String = 'main', 
											   minusActionBar:Boolean = false, minusTitleBar:Boolean = false, 
											   minusLogBar:Boolean = false, addLayout:Boolean = true) : Container
		{
			var container : Container = new Container();
			
			if (type == 'log') {
				container.setActualSize(stage.stageWidth, 260);
				container.x = stage.stageWidth > 720 ? 24 : 0;
				container.y = stage.stageHeight - 140 - 260 + 12;
				
				var spr:Sprite = new Sprite();
				spr.graphics.beginFill(0x333333, 0.7);
				spr.graphics.drawRoundRect(0,0,720,236, 20, 20);
				spr.graphics.beginFill(0x3a3a3a, 0.7);
				spr.graphics.drawRoundRect(4,4,712,228, 20, 20);
				spr.graphics.beginFill(0x2a2a2a, 0.7);
				spr.graphics.drawRoundRect(8,8,704,220, 20, 20);
				container.addChildAt(spr, 0);
				
				return container;
			}
			else if (type == 'logitemcontainer') {
				container.vScrollVisible = true;
				container.scrollDirection = ScrollDirection.VERTICAL;
				container.allowScrollPastEdge = true;
				
				var glay:GridLayout = new GridLayout(1);
				glay.padding = 6;
				glay.paddingLeft = 12;
				glay.paddingBottom = 24;
				//rlay.justify = true; //default is false;
				container.layout = glay;
				return container;
			}
			else if (type == 'logitem') {
				var gglay:GridLayout = new GridLayout(2);
				var gridData:GridData = new GridData();
				gridData.marginBottom = 16;
				container.layout = gglay;
				container.layoutData = gridData;
				return container;
			}
			//var layout : RowLayout = new RowLayout();
			if (addLayout) {
				var layout : GridLayout = new GridLayout( 1 );
				if (type == 'main') {
					layout.paddingLeft = layout.paddingRight = 24;
					layout.paddingTop = 32;
					layout.spacing = 5;
				}
				else {
					layout.paddingBottom = 32;
				}
				container.layout = layout;
			
				//var containerData : GridData = new GridData();
				//containerData.vAlign = Align.END;
				var containerData:GridData = new GridData();
				containerData.hAlign = Align.BEGIN;
				containerData.vAlign = Align.BEGIN;
				
				//containerData.setOptions( SizeOptions.RESIZE_BOTH );
				container.layoutData = containerData;
			}
			
			
			if (type == 'main') {
				container.setActualSize(stage.stageWidth, stage.stageHeight - (minusActionBar ? 140 : 0) - (minusTitleBar ? 111 : 0) - (minusLogBar ? 260 : 0));
				container.y = minusTitleBar ? 111 : 0;
			}
			else {
				container.width = 720;
				container.x = stage.stageWidth > 720 ? 24 : 0;
				container.setActualSize(stage.stageWidth - 48, 720);
			}
			
			return container;
		}
	}
}
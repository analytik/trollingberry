package uglycode.trollingberry.view
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.describeType;
	
	import qnx.display.IowWindow;
	import qnx.fuse.ui.buttons.CheckBox;
	import qnx.fuse.ui.buttons.LabelButton;
	import qnx.fuse.ui.core.Container;
	import qnx.fuse.ui.dialog.AlertDialog;
	import qnx.fuse.ui.dialog.DialogBase;
	import qnx.fuse.ui.layouts.Align;
	import qnx.fuse.ui.layouts.gridLayout.GridData;
	import qnx.fuse.ui.layouts.gridLayout.GridLayout;
	import qnx.fuse.ui.layouts.gridLayout.GridRowData;
	import qnx.fuse.ui.layouts.rowLayout.RowData;
	import qnx.fuse.ui.layouts.rowLayout.RowLayout;
	import qnx.fuse.ui.listClasses.DropDown;
	import qnx.fuse.ui.listClasses.ListSelectionMode;
	import qnx.fuse.ui.listClasses.ScrollDirection;
	import qnx.fuse.ui.listClasses.SectionList;
	import qnx.fuse.ui.picker.Picker;
	import qnx.fuse.ui.text.Label;
	import qnx.fuse.ui.text.TextFormat;
	import qnx.fuse.ui.text.TextInput;
	import qnx.ui.data.DataProvider;
	import qnx.ui.data.SectionDataProvider;
	
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.TrollingBerry;
	import uglycode.trollingberry.util.PoolThingy;
	
	public class MainMenuView extends Container
	{
		private var topContainer:Container;
		private var topContainerNewGameSelection:Container;
		private var mainContainer:Container;
		private var myRow:RowLayout;
		private var myRowD:RowData;
		private var mySectionList2:SectionList;
		private var mySectionList:SectionList;
		private var alert:AlertDialog;
		
		private var randomNames:Array = [
			'TurboKokot2000',
			'rambo1993',
			'strong_sad',
			'magnolia_fan'
			//todo add more jokes from strong bad emails and jay & silent bob
			];
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function MainMenuView()
		{
			super();
			trace('mainmenu view constructor');
		}
		

		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		override protected function init():void {
			super.init();
			trace ('initing MainMenu');
			this.addEventListener(Event.ADDED_TO_STAGE, init2);
		}
		
		
		protected function init2(e:Event):void {
			this.removeEventListener(Event.ADDED_TO_STAGE, init2);
			topContainer = new Container();
			topContainer.setActualSize(this.width, this.height);
			this.addChild(topContainer);
			
			var topGrid:GridLayout = new GridLayout();
			topContainer.layout = topGrid;
			topGrid.numColumns = 1;
			
			/*
			var myCheckBox:CheckBox = new CheckBox();
			myCheckBox.setPosition(175, 350);
			myCheckBox.width = 200;
			myCheckBox.label = "Disable login";
			//myCheckBox.addEventListener(MouseEvent.CLICK, handleCheckboxClick);
			topContainer.addChild(myCheckBox);
			*/
			
			var myButton:LabelButton = new LabelButton();
			myButton.setPosition(175, 300);
			myButton.width = 600;
			myButton.label = "New Game";
			myButton.addEventListener(MouseEvent.CLICK, flipToNewGameUI);
			topContainer.addChild(myButton);
		}
		
		
		
		private function initializeNewGameUI():void
		{
			//this.createContainer(); return;
			topContainerNewGameSelection = new Container();
			topContainerNewGameSelection.scrollDirection = ScrollDirection.VERTICAL;
			
			topContainerNewGameSelection.setActualSize(this.width, this.height);
			
			var topGrid:GridLayout = new GridLayout();
			topGrid.horizontalPadding = 16;
			topGrid.verticalPadding = 32;
			topContainerNewGameSelection.layout = topGrid;
			topGrid.numColumns = 2;
			
			/**
			 * Top label
			 */
			var lbl:Label = new Label();            
			lbl.name = "Title";
			var format : TextFormat = PoolThingy.getTextFormat();
			lbl.format = format;
			lbl.text = 'Create your character';
			
			var topTitleLayoutData:GridData = new GridData();
			topTitleLayoutData.preferredWidth = this.width - topGrid.paddingLeft - topGrid.paddingRight;
			topTitleLayoutData.hAlign = Align.CENTER;
			topTitleLayoutData.hSpan = 2;
			topTitleLayoutData.marginBottom = 24;
			lbl.layoutData = topTitleLayoutData;
			
			topContainerNewGameSelection.addChild(lbl);
			
			/**
			 * Creating the ethnicity SectionList 
			 */
			var arrEthnicity:Array = [
				{label: "Hispanic / Latin"},
				{label: "Asian"},
				{label: "Native American"},
				{label: "Indian"},
				{label: "Middle Eastern"},
				{label: "Pacific Islander"},
				{label: "Black"},
				{label: "White"},
				{label: "Other"}
			];
			
			//var monthDP:DataProvider=new DataProvider(arrMonth);
			
			var mySDP:SectionDataProvider = new SectionDataProvider();
			
			var section:Object = {label:'Enthnicity'};
			mySDP.addItem( section );
			
			for( var j:int = 0; j < arrEthnicity.length; j++ )
			{
				mySDP.addChildToItem( {label: arrEthnicity[j].label}, section );
			}
			
			mySectionList = new SectionList(); 
			mySectionList.selectionMode = ListSelectionMode.SINGLE;
			mySectionList.headerHeight = 48;
			mySectionList.dataProvider = mySDP;
			//topContainerNewGameSelection.addChild(mySectionList);
			
			var myGridData:GridData = new GridData();
			myGridData.preferredHeight = 940;
			myGridData.preferredWidth = 360;
			myGridData.vAlign = Align.CENTER;
			
			mySectionList.layoutData = myGridData;            
			topContainerNewGameSelection.addChild(mySectionList);
			
			/*
			this is almost useless, affects resizing behaviour
			var myGridRowData:GridRowData = new GridRowData();
			myGridRowData.grow = GridRowData.FORCE_ON;
			topGrid.setColumnData(1, myGridRowData);*/
			
			/**
			 * gender selection
			 */
			//mainContainer = new Container();
			var mySDP2:SectionDataProvider = new SectionDataProvider();
			
			var section2:Object = {label:'Gender and age'};
			mySDP2.addItem( section2 );
			
			mySDP2.addChildToItem( {label: 'Male, teens'}, section2 );
			mySDP2.addChildToItem( {label: 'Female, teens'}, section2 );
			mySDP2.addChildToItem( {label: 'Male, early 20s'}, section2 );
			mySDP2.addChildToItem( {label: 'Female, early 20s'}, section2 );
			mySDP2.addChildToItem( {label: 'Male, late 20s'}, section2 );
			mySDP2.addChildToItem( {label: 'Female, late 20s'}, section2 );
			mySDP2.addChildToItem( {label: 'Male, 30s'}, section2 );
			mySDP2.addChildToItem( {label: 'Female, 30s'}, section2 );
			
			mySectionList2 = new SectionList(); 
			mySectionList2.selectionMode = ListSelectionMode.SINGLE;
			mySectionList2.headerHeight = 48;
			mySectionList2.dataProvider = mySDP2;
			//topContainerNewGameSelection.addChild(mySectionList);
			
			var myGridData2:GridData = new GridData();
			myGridData2.preferredHeight = 940;
			myGridData2.vAlign = Align.CENTER;
			mySectionList2.layoutData = myGridData2;
			
			topContainerNewGameSelection.addChild(mySectionList2);
			
			
			/**
			 * New Game Button
			 */
			var newGameButton:LabelButton = new LabelButton();            
			newGameButton.name = "Title2";
			//newGame.lformat = PoolThingy.getTextFormat();
			newGameButton.label = '>> Start game >>';
			newGameButton.layoutData = topTitleLayoutData;
			newGameButton.addEventListener(MouseEvent.CLICK, checkNewGameParams);
			
			topContainerNewGameSelection.addChild(newGameButton);
		}
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		protected function flipToNewGameUI(event:MouseEvent):void
		{
			if (!topContainerNewGameSelection) {
				this.initializeNewGameUI();
			}
			this.removeChild(topContainer);
			this.addChild(topContainerNewGameSelection);
		}		
		
		
		
		protected function alertButtonClickedNowCloseIt(event:Event):void
		{
			alert.cancel();
		}		
		
		
		
		protected function alertButtonClickedNowStartAGame(event:Event):void
		{
			alert.cancel();
			startANewGame();
		}
		
		
		
		protected function checkNewGameParams(event:MouseEvent):void
		{
			alert = new AlertDialog();
			if (!this.mySectionList.selectedItem || !this.mySectionList2.selectedItem) {
				alert.title = "Character selection";
				alert.message = "Please select your preferred ethnicity and gender. This is very important.";
				alert.addButton("OK");
				//alert.addButton("CANCEL");
				alert.addEventListener(Event.SELECT, alertButtonClickedNowCloseIt); 
				//this.addChild(alert);
				alert.show();
				trace('Please select your preferred ethnicity and gender. This is very important.');
				return;
			}
			if (this.mySectionList2.selectedItem.label == 'Male, early 20s' && this.mySectionList.selectedItem.label == 'White') {
				trace('Very well then.');
				startANewGame();
				return;
			}
			if (this.mySectionList2.selectedItem.label != 'Male, early 20s' || this.mySectionList.selectedItem.label != 'White') {
				alert.title = "Character selection";
				alert.message = "Nice try, buster. You're a 20-year old white, horny, single male. Yep. This is all going to be about #firstworldproblems.";
				alert.addButton("Uh...");
				alert.addEventListener(Event.SELECT, alertButtonClickedNowStartAGame); 
				//this.addChild(alert);
				alert.show();
			}
		}
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		
		private function startANewGame():void
		{
			ServiceLocator.getInstance().player.newGame();
			TrollingBerry.getInstance().switchStateTo('game');
		}
		
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

		
		
		[Deprecated]
		private function createContainer():void
		{
			var container:Container = new Container();
			container.scrollDirection = ScrollDirection.VERTICAL;  
			container.height = stage.stageHeight;
			container.width = stage.stageWidth;
			
			var grid:GridLayout = new GridLayout();
			container.layout = grid;
			grid.spacing = 5;
			
			grid.padding = 10;
			grid.numColumns = 2;
			grid.debugId = "creditCardGrid";
			
			var titleData:GridData = new GridData();
			titleData.hSpan = 4;
			titleData.hAlign = Align.FILL;
			titleData.vAlign = Align.END;
			
			var spacerData:GridData = new GridData();
			spacerData.hSpan = 4;
			spacerData.preferredHeight = 20;
			
			var secPinData:GridData = new GridData();
			secPinData.hAlign = Align.BEGIN;
			secPinData.vAlign = Align.CENTER;
			secPinData.preferredWidth = 150;
			
			var textInputAlignData:GridData = new GridData();
			textInputAlignData.vAlign = Align.CENTER;
			textInputAlignData.hAlign = Align.FILL;
			textInputAlignData.preferredWidth = 200;
			
			var labelAlignData:GridData = new GridData();
			labelAlignData.hAlign = Align.END;
			labelAlignData.vAlign = Align.CENTER;
			
			var ccSpacer:Label = new Label();
			ccSpacer.layoutData = spacerData;
			
			var ccInfoLabel:Label = new Label();
			ccInfoLabel.text = "Credit Card Information";
			ccInfoLabel.layoutData = titleData;
			container.addChild(ccInfoLabel);
			
			var ccInfoFNameLabel:Label = new Label();
			ccInfoFNameLabel.text = "FIRST NAME";
			ccInfoFNameLabel.layoutData = labelAlignData;
			container.addChild(ccInfoFNameLabel);
			
			var ccInfoFNameInput:TextInput = new TextInput();
			ccInfoFNameInput.layoutData = textInputAlignData;
			container.addChild(ccInfoFNameInput);
			
			var ccInfoLNameLabel:Label = new Label();
			ccInfoLNameLabel.text = "LAST NAME";
			ccInfoLNameLabel.layoutData = labelAlignData;
			container.addChild(ccInfoLNameLabel);
			
			var ccInfoLNameInput:TextInput = new TextInput();
			ccInfoLNameInput.layoutData = textInputAlignData;
			container.addChild(ccInfoLNameInput);
			
			var ccInfoCCTypeLabel:Label = new Label();
			ccInfoCCTypeLabel.text = "CARD TYPE";
			ccInfoCCTypeLabel.layoutData = labelAlignData;
			container.addChild(ccInfoCCTypeLabel);
			
			var arrCards:Array=[];
			arrCards.push({label: "VISA"});
			arrCards.push({label: "MasterCard"});
			var ccInfoCCTypeDropDown:DropDown = new DropDown();    
			ccInfoCCTypeDropDown.prompt = "Select Card";
			ccInfoCCTypeDropDown.dataProvider = new DataProvider(arrCards);
			ccInfoCCTypeDropDown.measureContents = true;
			
			ccInfoCCTypeDropDown.layoutData = textInputAlignData;
			container.addChild(ccInfoCCTypeDropDown);
			
			var ccInfoCardNumberLabel:Label = new Label();
			ccInfoCardNumberLabel.text = "CARD NUMBER";
			ccInfoCardNumberLabel.layoutData = labelAlignData;
			container.addChild(ccInfoCardNumberLabel);
			
			var ccInfoCardNumberInput:TextInput = new TextInput();
			ccInfoCardNumberInput.layoutData = textInputAlignData;
			container.addChild(ccInfoCardNumberInput);
			
			var ccInfoCCExp:Label = new Label();
			ccInfoCCExp.text = "EXPIRATION";
			ccInfoCCExp.layoutData = labelAlignData;
			container.addChild(ccInfoCCExp);
			
			{
				var expComp:Container = new Container();
				var expGrid:GridLayout = new GridLayout();
				expGrid.hAlign = Align.FILL;
				expComp.layout = expGrid;
				expGrid.numColumns = 0;
				expGrid.spacing = 5;
				
				var arrMonths:Array=[];
				arrMonths.push({label: "1"});
				arrMonths.push({label: "2"});
				arrMonths.push({label: "3"});
				arrMonths.push({label: "4"});
				arrMonths.push({label: "5"});
				arrMonths.push({label: "6"});
				arrMonths.push({label: "7"});
				arrMonths.push({label: "8"});
				arrMonths.push({label: "9"});
				arrMonths.push({label: "10"});
				arrMonths.push({label: "11"});
				arrMonths.push({label: "12"});
				var months:DataProvider = new DataProvider(arrMonths);
				
				var arrYears:Array=[];
				arrYears.push({label: "2013"});
				arrYears.push({label: "2014"});
				arrYears.push({label: "2015"});
				arrYears.push({label: "2016"});
				arrYears.push({label: "2017"});
				arrYears.push({label: "2018"});
				arrYears.push({label: "2019"});
				arrYears.push({label: "2020"});
				arrYears.push({label: "2021"});
				arrYears.push({label: "2022"});
				arrYears.push({label: "2023"});
				arrYears.push({label: "2024"});
				arrYears.push({label: "2025"});
				arrYears.push({label: "2026"});
				var years:DataProvider = new DataProvider(arrYears);
				
				var expData:DataProvider = new DataProvider();
				expData.addItem(months);
				expData.addItem( years );
				
				var expPicker:Picker = new Picker();
				expPicker.dataProvider = expData;
				expPicker.prompt = "Month/Year";
				expComp.addChild( expPicker );
				expPicker.valueFunction = function(selectedItems:Array):String {
					return selectedItems[0]['label'] + "/" + selectedItems[1]['label'];
				};
				
				container.addChild(expComp);
			}
			
			var ccInfoSecPinLabel:Label = new Label();
			ccInfoSecPinLabel.text = "SECURITY PIN";
			ccInfoSecPinLabel.layoutData = labelAlignData;
			container.addChild(ccInfoSecPinLabel);
			
			var ccInfoSecPinInput:TextInput = new TextInput();
			ccInfoSecPinInput.layoutData = secPinData;
			container.addChild(ccInfoSecPinInput);
			
			ccSpacer = new Label();
			ccSpacer.layoutData = spacerData;
			container.addChild(ccSpacer);          
			
			var ccAddrLabel:Label = new Label();
			ccAddrLabel.text = "Credit Card Billing Address";
			ccAddrLabel.layoutData = titleData;
			container.addChild(ccAddrLabel);
			
			var ccAddrStreetLabel:Label = new Label();
			ccAddrStreetLabel.text = "STREET ADDRESS";
			ccAddrStreetLabel.layoutData = labelAlignData;
			container.addChild(ccAddrStreetLabel);
			
			var ccAddrStreetInput:TextInput = new TextInput();
			ccAddrStreetInput.layoutData = textInputAlignData;
			container.addChild(ccAddrStreetInput);
			
			var ccAddrCityLabel:Label = new Label();
			ccAddrCityLabel.text = "CITY";
			ccAddrCityLabel.layoutData = labelAlignData;
			container.addChild(ccAddrCityLabel);
			
			var ccAddrCityInput:TextInput = new TextInput();
			ccAddrCityInput.layoutData = textInputAlignData;
			container.addChild(ccAddrCityInput);
			
			var ccAddrCountryLabel:Label = new Label();
			ccAddrCountryLabel.text = "COUNTRY";
			ccAddrCityLabel.layoutData = labelAlignData;
			container.addChild(ccAddrCountryLabel);
			
			var arrCountries:Array=[];
			arrCountries.push({label: "Canada"});
			arrCountries.push({label: "Madagascar"});
			var ccAddrCountriesDropDown:DropDown = new DropDown();
			ccAddrCountriesDropDown.prompt = "Canada";
			ccAddrCountriesDropDown.dataProvider = new DataProvider(arrCountries);
			
			ccAddrCountriesDropDown.layoutData = textInputAlignData;
			container.addChild(ccAddrCountriesDropDown);
			
			var ccAddrStreet2Label:Label = new Label();
			ccAddrStreet2Label.text = "POSTAL/ZIP CODE";
			ccAddrStreet2Label.layoutData = labelAlignData;
			container.addChild(ccAddrStreet2Label);
			
			var ccAddrStreet2Input:TextInput = new TextInput();
			ccAddrStreet2Input.layoutData = textInputAlignData;
			container.addChild(ccAddrStreet2Input);
			
			this.addChild(container);
			
		}
	}
}
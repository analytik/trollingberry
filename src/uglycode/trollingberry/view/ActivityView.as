package uglycode.trollingberry.view
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	
	import qnx.fuse.ui.actionbar.ActionBar;
	import qnx.fuse.ui.actionbar.ActionPlacement;
	import qnx.fuse.ui.core.Action;
	import qnx.fuse.ui.core.Container;
	import qnx.fuse.ui.display.Image;
	import qnx.fuse.ui.events.ActionEvent;
	import qnx.fuse.ui.events.ListEvent;
	import qnx.fuse.ui.layouts.Align;
	import qnx.fuse.ui.layouts.gridLayout.GridData;
	import qnx.fuse.ui.layouts.gridLayout.GridLayout;
	import qnx.fuse.ui.layouts.rowLayout.RowData;
	import qnx.fuse.ui.listClasses.List;
	import qnx.fuse.ui.listClasses.ListSelectionMode;
	import qnx.fuse.ui.listClasses.RoundList;
	import qnx.fuse.ui.listClasses.ScrollDirection;
	import qnx.fuse.ui.skins.text.TextInputSkin;
	import qnx.fuse.ui.text.Label;
	import qnx.fuse.ui.text.TextArea;
	import qnx.fuse.ui.text.TextFormat;
	import qnx.fuse.ui.text.TextTruncationMode;
	import qnx.fuse.ui.titlebar.TitleBar;
	import qnx.fuse.ui.titlebar.TitleBarStyle;
	import qnx.ui.data.DataProvider;
	
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.TrollingBerry;
	import uglycode.trollingberry.game.ActionLogBit;
	import uglycode.trollingberry.game.GameTime;
	import uglycode.trollingberry.util.PoolThingy;
	import uglycode.trollingberry.view.ui.Utils;
	import uglycode.trollingberry.world.actions.Action;
	import uglycode.trollingberry.world.activities.Activity;
	
	public class ActivityView extends Container
	{
		private var a:Activity;
		private var _controlContainer:Container;
		private var centeredGridData:GridData;
		private var labelGridData:GridData;
		private var progressGridData:GridData;
		private var activityContainer:Container;
		private var gridLayout:GridLayout;
		private var imgGridData:GridData;
		private var actions:Object;
		private var player:Player;
		private var actionList:List;

		private var logContainer:Container;
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function ActivityView()
		{
			//trace('in activity view constructor');
			super();
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		override protected function init():void {
			//trace('in activity view init');
			super.init();
			this.a = ServiceLocator.getInstance().player.currentActivity;
			this.player = ServiceLocator.getInstance().player;
			this.addEventListener(Event.ADDED_TO_STAGE, init2);
		}
		
		
		
		protected function init2(event:Event):void
		{
			// ACTION BAR
			this.removeEventListener(Event.ADDED_TO_STAGE, init2);
			//ab.backButton = new qnx.fuse.ui.core.Action('', null, null, 'default', ['a']);
			var ab:ActionBar = PoolThingy.getEmptyActionBar();
			ab.enableBackButtonDrag = false;
			ab.addAction(new qnx.fuse.ui.core.Action('Stats', new Bitmap(new BitmapData(128,128,false,0)), {}, ActionPlacement.ON_BAR));
			ab.backButton = new qnx.fuse.ui.core.Action('Back');
			ab.addEventListener(qnx.fuse.ui.events.ActionEvent.ACTION_SELECTED, handleActionBarClicks);
			this.addChild(ab);

			// TITLE BAR
			var titlebar : TitleBar = new TitleBar(TitleBarStyle.DEFAULT);
			titlebar.title = a.name;
			//titlebar.acceptAction = new Action( "Accept" ); //TODO view Stats? or in ActionBar?
			//titlebar.dismissAction = new Action( "Cancel" );
			titlebar.addEventListener( ActionEvent.ACTION_SELECTED, function(e:Event):void{trace('Selected something on a title bar? Huh.')} );
			addChild( titlebar );
			
			activityContainer = Utils.createContainer(this.stage, 'main', true, true, true, false);
			activityContainer.vScrollVisible = true;
			activityContainer.scrollDirection = ScrollDirection.VERTICAL;
			activityContainer.allowScrollPastEdge = true;
			//activityContainer.scrollBarAlpha = 0.8;
			//activityContainer.validateNow();


			gridLayout = new GridLayout();
			gridLayout.horizontalPadding = 16;
			gridLayout.verticalPadding = 32;
			gridLayout.numColumns = 3;
			activityContainer.layout = gridLayout;
			
			var fullWidthLayoutData:GridData = new GridData();
			fullWidthLayoutData.preferredWidth = activityContainer.width - gridLayout.paddingLeft - gridLayout.paddingRight;
			fullWidthLayoutData.hAlign = Align.CENTER;
			fullWidthLayoutData.hSpan = 3;
			fullWidthLayoutData.marginBottom = 24;
			
			/**
			 * TODO MOVE THIS INTO gAMEvIEW, and fuck around with making sure clickability is okay and crap like that.
			 * Site description
			 */
			imgGridData = new GridData();
			imgGridData.marginRight = 24; 
			imgGridData.marginLeft = 12;
			imgGridData.marginBottom = 12;
			imgGridData.preferredWidth = 128;
			imgGridData.hAlign = Align.CENTER;
			imgGridData.vAlign = Align.CENTER;
			
			var img:Image = new Image();
			img.setImage(a.icon);
			img.layoutData = imgGridData;
			activityContainer.addChild(img);
			
			var descriptionLayoutData:GridData = new GridData();
			descriptionLayoutData.preferredWidth = (activityContainer.width - gridLayout.paddingLeft - gridLayout.paddingRight) - 128 - 12 - 24;
			descriptionLayoutData.hAlign = Align.BEGIN;
			descriptionLayoutData.hSpan = 2;
			descriptionLayoutData.marginBottom = 24;
			
			
			//var desc:TextArea = new TextArea();
			var desc:Label = new Label();
			//desc.editable = desc.selectable = false;
			desc.selectable = false;
			desc.format = PoolThingy.getTextFormat('desc');
			desc.minLines = 2;
			desc.maxLines = 0;
			desc.text = a.description;
			//desc.setActualSize(500,500);
			desc.layoutData = descriptionLayoutData;
			/***
			 * END TODO
			 */
			
			activityContainer.addChild(desc);
			
			
			/**
			 * List of actions
			 */
			var lbl:Label = new Label();
			lbl.name = "actions title";
			lbl.format = PoolThingy.getTextFormat('default');
			lbl.text = 'Available actions:';
			lbl.layoutData = fullWidthLayoutData;
			activityContainer.addChild(lbl);
			
			actionList = new List();
			actionList.selectionMode = ListSelectionMode.NONE;        
			actionList.layoutData = fullWidthLayoutData.clone();
			//(actionList.layoutData as GridData).vSpan = 4;
			actionList.addEventListener(ListEvent.ITEM_CLICKED, handleActionClicked);
			actionList.scrollable = false;
			
			this.refreshActionsForList(actionList);
			
			activityContainer.addChild(actionList);
			
			
			/**
			 * Status report (basic info, without reputation stats)
			 * TODO: 
			 * - Followers
			 * - status, etc... just read it from IAttribProvider.getVisibleAttribs():Dictionary
			 */
			/*
			var lbl2:Label = new Label();
			lbl2.name = "status title";
			lbl2.format = PoolThingy.getTextFormat('default');
			lbl2.text = 'Your status on ' + a.name + ':';
			lbl2.layoutData = fullWidthLayoutData;
			activityContainer.addChild(lbl2);
			
			var lbl3:Label = new Label();
			lbl3.name = "status title2";
			lbl3.format = PoolThingy.getTextFormat('desc');
			lbl3.maxLines = 20;
			lbl3.text = 'Here' + ' is a nice little story about how nobody likes you on this site. There is not much to say - you\'re a big jerk, you know nothing about anything, your troll attempts are weak. ' +
				'Also, some other text lorem ipsum dolor sit amet bla buh rei dhjoks ofmls fuckheads titmuffins shitcakes lozenges headaches shittty speakers and dinosaurs.' +
				' is a nice little story about how nobody likes you on this site. There is not much to say - you\'re a big jerk, you know nothing about anything, your troll attempts are weak. ' +
				'Also, some other text lorem ipsum dolor sit amet bla buh rei dhjoks ofmls fuckheads titmuffins shitcakes lozenges headaches shittty speakers and dinosaurs.';
			lbl3.layoutData = fullWidthLayoutData;
			activityContainer.addChild(lbl3);
			*/

			addChild(activityContainer);

			logContainer = Utils.createContainer(stage, 'log', true);
			addChild(logContainer);
			this.refreshLogContainer();
		}
		

		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		protected function handleActionClicked(event:ListEvent):void
		{
			//trace('Running activity ' + event.data.id + ' --- ' + event.data.label);
			// TODO if greyed out, 
			player.doAction(event.data.action);
			refreshActionsForList(actionList);
			refreshLogContainer();
		}
		
		
		private function refreshLogContainer():void
		{
			try {
				logContainer.removeChildAt(1);
			} catch (e) {}
			
			var log:Vector.<ActionLogBit> = ServiceLocator.getInstance().player.actionLog.log;
			//todo: order descending, limit 7-15? depending on player memory?
			//todo move elsewhere / static
			var realLogItemContainer:Container = Utils.createContainer(stage, 'logitemcontainer');
			realLogItemContainer.y += 8;
			realLogItemContainer.width = 700;
			realLogItemContainer.height = 220;
			logContainer.addChildAt(realLogItemContainer, 1);

			var titleStyle:GridData = new GridData;
			titleStyle.hAlign = Align.BEGIN;
			var descStyle:GridData = new GridData;
			descStyle.hAlign = Align.BEGIN;
			descStyle.hSpan = 2;
			descStyle.preferredWidth = 670;
			var timeStyle:GridData = new GridData();
			timeStyle.hAlign = Align.END;
			timeStyle.marginRight = 20;
			//micro-optimisation
			var i:int;
			var len:int = log.length;
			
			for (i = len-1; i >= len - 7 && i >= 0; i--)
			{
				var bit:ActionLogBit = log[i];
				var itemContainer:Container = Utils.createContainer(stage, 'logitem');

				var logItemTitle:Label = new Label;
				logItemTitle.htmlText = bit.fullName;
				logItemTitle.maxLines = 2;
				logItemTitle.layoutData = titleStyle;
				logItemTitle.format = PoolThingy.getTextFormat('smalltitle');
				itemContainer.addChild(logItemTitle);
				
				var logItemTime:Label = new Label;
				logItemTime.text = GameTime.readable(bit.value);
				logItemTime.layoutData = timeStyle;
				logItemTime.format = PoolThingy.getTextFormat('smalltime');
				itemContainer.addChild(logItemTime);
				
				var logItemDesc:Label = new Label;
				logItemDesc.maxLines = 10;
				logItemDesc.text = bit.message;
				logItemDesc.layoutData = descStyle;
				logItemDesc.format = PoolThingy.getTextFormat('desc');
				itemContainer.addChild(logItemDesc);
				
				realLogItemContainer.addChild(itemContainer);
			}
		}
		
		
		protected function handleActionBarClicks(event:ActionEvent):void
		{
			if (event.action.label == 'Back') {
				TrollingBerry.getInstance().switchStateTo('game');
			}
			else if (event.action.label == 'Stats') {
				//trace ('click on Stats');
				TrollingBerry.getInstance().switchStateTo('stats');
			}
		}
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		
		
		private function refreshActionsForList(myList:List):void
		{
			var arrMonth:Array=[];
			//re-init actions because we need to reset the condition bits... derp.
			a.initActions();
			actions = a.getAvailableActions();
			for (var i:int = 0; i < actions.length; i++) 
			{
				arrMonth.push({id: actions[i].ident, action: actions[i], label: actions[i].name});
				//trace ('adding activity ' + actions[i].ident + ' -- ' + actions[i].name);
			}
			myList.dataProvider = new DataProvider(arrMonth);
			myList.validateNow();
			
			/*arrMonth.push({id: 2, label: "Apples"});
			arrMonth.push({id: 3, label: "Bacon"});
			arrMonth.push({id: 4, label: "Hot dogs"});
			arrMonth.push({id: 5, label: "Celery"});*/
		}		

		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
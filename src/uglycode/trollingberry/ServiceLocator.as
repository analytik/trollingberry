package uglycode.trollingberry
{
	import flash.errors.IllegalOperationError;
	
	import uglycode.trollingberry.character.Reputation;
	import uglycode.trollingberry.world.Topics;

	public class ServiceLocator
	{
		private static var _instance:ServiceLocator;
		public var player:Player;
		public var topics:Topics
		//public var reputation:Reputation; // domain specific
		
		public function ServiceLocator()
		{
			if (_instance) {
				throw new IllegalOperationError("Singleton already constructed!");
			}
			//loading a game will be managed through UI / at the app startup
			topics = new Topics;
			//player = Player.getInstance();
		}
		
		public static function getInstance():ServiceLocator {
			if (_instance) {
				return _instance;
			}
			_instance = new ServiceLocator();
			return _instance;
		}
		
		public function getPlayer():Player
		{
			return this.player;
		}
	}

}
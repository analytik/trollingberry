package uglycode.trollingberry
{
	import flash.net.SharedObject;
	

	/**
	 * A class for storing data about player.
	 */
	public class PlayerSave
	{
		public function PlayerSave()
		{
		}
		
		
		
		private function generateSaveName(player:Player):String {
			return 'save_' + player.name + '_' + player.creationDate;
		}
		
		
		
		private function generateSaveIndex(player:Player):String {
			return 'index_' + player.name;
		}

		
	
		public function saveSavegame(player:Player) : void
		{
			var savename:String = this.generateSaveName(player);
			var so : SharedObject = SharedObject.getLocal( savename );
			//save all player data here
			so.data['warpSpeed'] = 12;
			so.flush();
			
			//and then update save list (to be used later for multiple savegames)
			var indexSave:SharedObject = SharedObject.getLocal(generateSaveIndex(player));
			if (indexSave == null) {
				return;
			}
			if (!indexSave.data.hasOwnProperty('list') || !(indexSave.data['list'] is Array)) {
				indexSave['list'] = [];
			}
			(indexSave.data['list'] as Array).push(savename);
			indexSave.data['lastSaveName'] = savename;
			indexSave.flush();
		}
		
		
		
		public function loadLastSavegame(player:Player):Player {
			var indexSave:SharedObject = SharedObject.getLocal(generateSaveIndex(player));
			if (!indexSave) {
				//no saved game, return player as it was
				return player;
			}
			var saveName:String = indexSave['lastSaveName'];
			
			return loadSavegame(saveName, player);
		}
		
		
		
		//todo turn public once player can choose individual savegames
		private function loadSavegame(saveName:String, player:Player) : Player
		{
			var so : SharedObject = SharedObject.getLocal( saveName );
			if (so)
			{
				var value:Number = so.data['warpSpeed'];
			}
			return player;
		}
	}
}
package uglycode.trollingberry
{
	import flash.desktop.NativeApplication;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	
	import qnx.fuse.ui.core.Container;
	import qnx.fuse.ui.theme.ThemeGlobals;
	import qnx.system.FontSettings;
	
	import uglycode.trollingberry.view.ActivityView;
	import uglycode.trollingberry.view.CreditsView;
	import uglycode.trollingberry.view.GameView;
	import uglycode.trollingberry.view.MainMenuView;
	import uglycode.trollingberry.view.StatsView;
	
	[SWF(backgroundColor="#272727", frameRate="60")]
	public class TrollingBerry extends Sprite
	{
		[Embed(source="../../../assets/styles/styles.css", mimeType="application/octet-stream")]
		private var STYLES : Class;
		public static var mainMenuView : MainMenuView;
		public static var gameView : GameView;
		public static var statsView : StatsView;
		public static var activityView : ActivityView;
		public static var creditsView:CreditsView;
		//public static var mainView : StarshipSettingsView;
		public static var currentView:Container;
		private var retardResizingFlag:Boolean = false;
		private static var _instance:TrollingBerry;
		
		
		
		public function TrollingBerry()
		{
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.addEventListener( Event.RESIZE, firstStageResize );
			
			NativeApplication.nativeApplication.addEventListener( Event.EXITING, onApplicationExiting );
			
			ThemeGlobals.currentTheme = ThemeGlobals.BLACK;
			ThemeGlobals.injectCSS( (new STYLES() as ByteArray).toString() );
			//_mainView = new StarshipSettingsView();
		}
		
		
		public static function getInstance():TrollingBerry {
			return TrollingBerry._instance;
		}
		
		
		private function onApplicationExiting(event : Event) : void
		{
			trace ('Quitting');
			//_mainView.saveSettings();
		}
		
		
		
		private function firstStageResize(event : Event) : void
		{
			_instance = this;
			stage.removeEventListener( Event.RESIZE, firstStageResize );
			
			setFont();
			
			var timer:Timer = new Timer(500, 1); //0.5s should be enough?
			timer.start();
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, resumeResizingEventHandling);
			
			ServiceLocator.getInstance().player = Player.getInstance();
			//TEMP REMOVE AFTER MAIN MENU IS IN PLACE
			ServiceLocator.getInstance().player.newGame();
			
			switchStateTo('game');
			trace('resizing event - ' + stage.stageWidth + 'x' + stage.stageHeight);
		}
		
		private function setFont():void
		{
			//lol no
		}		
		
		
		protected function resumeResizingEventHandling(event:TimerEvent):void
		{
			stage.addEventListener( Event.RESIZE, stageResize );
		}
		
		
		
		protected function stageResize(event:Event):void
		{
			trace('---resizing event---');
			// TODO if landscape, apologize for not having a landscape UI.
		}
		
		public function switchStateTo(newState:String):void
		{
			if (currentView) {
				removeChild(currentView);
				currentView = null;
			}
			if (newState == 'game') {
				currentView = gameView = new GameView();
			}
			else if (newState == 'activity') {
				currentView = activityView = new ActivityView();
			}
			else if (newState == 'stats') {
				currentView = statsView = new StatsView();
			}
			else if (newState == 'credits') {
				currentView = creditsView = new CreditsView();
			}
			else if (newState == 'mainmenu') {
				currentView = mainMenuView = new MainMenuView();
			}
			else {
				throw new Error('No view to display! beep beep beep!');
			}
			addChild( currentView );
			currentView.setActualSize( stage.stageWidth, stage.stageHeight );
		}
	}	
	
}
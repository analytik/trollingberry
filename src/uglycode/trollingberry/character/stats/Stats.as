package uglycode.trollingberry.character.stats
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.IStatProvider;

	public class Stats implements IStatProvider
	{
		private var statDb:Dictionary = new Dictionary();
		
		public static var CHARISMA:String = 'Charisma';
		public static var PATIENCE:String = 'Patience';
		public static var CONCENTRATION:String = 'Concentration';
		public static var LONG_MEMORY:String = 'Long term memory';
		public static var SHORT_MEMORY:String = 'Short term memory';
		public static var AGILITY:String = 'Agility';
		public static var STRENGTH:String = 'Strength';
		
		
		public function Stats()
		{
			initStats();
		}
		
		private function initStats():void
		{
			statDb[CHARISMA] = new Stat(CHARISMA);
			statDb[PATIENCE] = new Stat(PATIENCE);
			statDb[CONCENTRATION] = new Stat(CONCENTRATION);
			statDb[LONG_MEMORY] = new Stat(LONG_MEMORY);
			statDb[SHORT_MEMORY] = new Stat(SHORT_MEMORY);
			statDb[AGILITY] = new Stat(AGILITY);
			statDb[STRENGTH] = new Stat(STRENGTH);
		}
		
		
		
		public function setDefaults(difficulty:String = 'normal', deviation:Number = 2.0):void {
			//if difficulty == normal then bla bla 
			statDb[CHARISMA].value = 5 + deviate(deviation);
			statDb[PATIENCE].value = 1 + deviate(deviation);
			statDb[CONCENTRATION] = -3 + deviate(deviation);
			statDb[LONG_MEMORY] = 5 + deviate(deviation);
			statDb[SHORT_MEMORY] = 7 + deviate(deviation);
			statDb[AGILITY] = 4 + deviate(deviation);
			statDb[STRENGTH] = 4 + deviate(deviation);
		}
		
		
		
		private function deviate(x:Number):Number {
			return (Math.random() * x) * (Math.random() < 0.5 ? 1 : -1); 
		}
		
		
		public function getVerbalType():String {
			return 'stat';
		}
		public function getStats():Dictionary {
			return statDb;
		}
		public function isMehSkillDescriptionVisible():Boolean {
			return false;
		}
		public function isGaugeDisplayed():Boolean {
			return true;
		}
		public function getStatVisibilityMargin():Number {
			return -20.0;
		}
		public function getStatVisibilityDeadzone():Number {
			return 3;
		}
		public function getStatTitle():String {
			return 'Your stats, like in an RPG:';
		}
		
		private function tempRandomizeStats():void {
			for (var s:String in statDb) {
				statDb[s].value = (Math.random()*40) - 20;
			}
		}
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
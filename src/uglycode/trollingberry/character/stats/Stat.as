package uglycode.trollingberry.character.stats
{
	import uglycode.trollingberry.game.IAffectable;
	import uglycode.trollingberry.game.INumericValueProvider;

	public class Stat implements IAffectable, INumericValueProvider
	{
		public var name:String;
		private var _value:Number;
		/**
		 * if someone has a special talent, increase this value. Special talents don't exist, so don't turn it up. Just lower it to annoy the fucker.
		 */
		public var factor:Number = 1.0;
		/**
		 * If someone is especially clumsy, just crank this shit up.
		 */
		public var factorNegative:Number = -1.0;

		
		
		
		public function Stat(name:String, value:Number = 0, factor:Number = 1.0, factorNeg:Number = -1.0)
		{
			this.name = name;
			this.value = value;
			this.factor = factor;
			this.factorNegative = factorNeg;
		}
		
		
		/**
		 * Enter positive number here to increase skill.
		 */
		public function increase(diff:Number):void {
			value = Math.min(value + (diff * factor), 20);
		}
		/**
		 * Don't forget to enter *positive* number here to decrease skill.
		 */
		public function decrease(diff:Number):void {
			value = Math.max(value + (diff * factorNegative), -20);
		}
		
		public function disable():void {}
		public function enable():void {}
		public function isEnabled():Boolean {
			return true;
		}

		public function get value():Number
		{
			return _value;
		}

		public function set value(value:Number):void
		{
			_value = value;
		}

	}
}
package uglycode.trollingberry.character
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.game.IDomainSpecific;
	import uglycode.trollingberry.game.IStatProvider;

	public class Reputation implements IStatProvider, IDomainSpecific
	{
		//public static const :String = '';
		public static const RESPECT:String = 'respectful';
		public static const SARCASM:String = 'sarcastic';
		public static const HUMOUR:String = 'funny';
		public static const ANAL:String = 'anal retentive';
		public static const BOREDOM:String = 'jaded';
		public static const NICE:String = 'nice';
		public static const INTELLIGENCE:String = 'smart';
		public static const SELF_ESTEEM:String = 'content';
		public static const SANITY:String = 'normal';
		public static const TOPIC_KNOWLEDGE1:String = '_default1_';
		public static const TOPIC_KNOWLEDGE2:String = '_default2_';
		public static const TOPIC_KNOWLEDGE3:String = '_default3_';
		
		[Deprecated]
		public var domainName:String;
		private var _domain:IDomain;
		
		
		public static var basicVocabulary:Dictionary = new Dictionary();
		public var repDb:Dictionary = new Dictionary;      //a mix of the following 2?
		public var topicRepDb:Dictionary = new Dictionary; //Vector.<TopicReputationBit> ???
		public var basicRepDb:Dictionary = new Dictionary; //Vector.<ReputationBit> ???
		
		public var parentDomain:IDomain;
		
		public function Reputation(parent:IDomain)
		{
			parentDomain = parent;
			initDictionary();
			initBasicReputation();
			tempRandomizeReputation();
		}

		
		private function initDictionary():void
		{
			/**
			 * Adjectives I'd like to use:
			 * witty
			 * hilarious
			 * pestersome (clinginess?)
			 * 
			 * TODO add adjectives for relationships / parents / friends / gaming
			 * TODO move to class VerbalDescription
			 */
			//basicVocabulary = new Dictionary();
			basicVocabulary[RESPECT] = [
				'a total dick',
				'an asshole',
				'pleasant',
				'respectful'
			];
			basicVocabulary[SARCASM] = [
				"someone who cannot take a joke",
				'unfunny',
				'sarcastic',
				'sarcastic bitch'
			];
			basicVocabulary[ANAL] = [
				"forgiving",
				'nice',
				'anal retentive',
				'insufferable prick'
			];
			basicVocabulary[BOREDOM] = [
				"interesting",
				'active',
				'bored',
				'jaded'
			];
			basicVocabulary[HUMOUR] = [
				'dense',
				'lame',
				'funny',
				'hilarious'
			];
			basicVocabulary[INTELLIGENCE] = [
				"obviously retarded",
				'dumb',
				'smart',
				'shrewd'
			];
			basicVocabulary[SELF_ESTEEM] = [
				"teeming with insecurities",
				'insecure',
				'content',
				'perfectly happy being yourself'
			];
			basicVocabulary[SANITY] = [
				"visibly insane",
				'disturbed',
				'',
				''
			];
			basicVocabulary[TOPIC_KNOWLEDGE1] = [
				"are clueless about",
				'get confused on the topic of', //too verbose?
				'want to get better in',
				'are an expert on'
			];
			basicVocabulary[TOPIC_KNOWLEDGE2] = [
				"are a premium retard when it comes to",
				'have problems understanding',
				'can talk about',
				'are a known guru of'
			];
			basicVocabulary[TOPIC_KNOWLEDGE3] = [
				"know nothing about",
				'know little about',
				'know stuff about',
				'know everything about'
			];
			basicVocabulary['unused'] = [
				"hate",
				'dislike',
				'like',
				'love'
			];
		}
		
		
		
		public function translate(name:String, level:uint = 2):String {
			if (basicVocabulary.hasOwnProperty(name) && basicVocabulary[name].hasOwnProperty(level)) {
				trace ('yes, it exists and it is ' + basicVocabulary[name][level]);
				return basicVocabulary[name][level];
			}
			//return defaults
			if (Math.random() > 0.34) {
				return basicVocabulary[TOPIC_KNOWLEDGE1][level];
			}
			else if (Math.random() > 0.33) {
				return basicVocabulary[TOPIC_KNOWLEDGE2][level];
			}
			else {
				return basicVocabulary[TOPIC_KNOWLEDGE3][level];
			}
			return null;
		}
		
		
		
		private function initBasicReputation():void
		{
			basicRepDb[RESPECT] = new ReputationBit(RESPECT, basicVocabulary[RESPECT], 0, this);
			basicRepDb[SARCASM] = new ReputationBit(SARCASM, basicVocabulary[SARCASM], 0, this);
			basicRepDb[ANAL] = new ReputationBit(ANAL, basicVocabulary[ANAL], 0, this);
			basicRepDb[BOREDOM] = new ReputationBit(BOREDOM, basicVocabulary[BOREDOM], 0, this);
			basicRepDb[HUMOUR] = new ReputationBit(HUMOUR, basicVocabulary[HUMOUR], 0, this);
			basicRepDb[NICE] = new ReputationBit(NICE, basicVocabulary[NICE], 0, this);
			basicRepDb[INTELLIGENCE] = new ReputationBit(INTELLIGENCE, basicVocabulary[INTELLIGENCE], 0, this);
			basicRepDb[SELF_ESTEEM] = new ReputationBit(SELF_ESTEEM, basicVocabulary[SELF_ESTEEM], 0, this);
			basicRepDb[SANITY] = new ReputationBit(SANITY, basicVocabulary[SANITY], 0, this);
		}
		
		
		
		private function tempRandomizeReputation():void
		{
			if (!parentDomain) return;
			if (!basicRepDb || basicRepDb.length == 0) return;
			for (var s:String in basicRepDb)
			{
				//clone all props
				repDb[s] = basicRepDb[s].clone();
				repDb[s].value = (Math.random()*40) - 20;
			}
			
			//now add random topical reputation.
			
		}
		
		
		
		public function getVerbalType():String {
			return 'reputation';
		}
		public function getStats():Dictionary {
			return repDb;
		}
		public function isMehSkillDescriptionVisible():Boolean {
			return false;
		}
		public function isGaugeDisplayed():Boolean {
			return false;
		}
		public function getStatVisibilityMargin():Number {
			return -20.0;
		}
		public function getStatVisibilityDeadzone():Number {
			return 3;
		}
		public function getStatTitle():String {
			return 'Reputation on ' + this.domain.name + ':';
		}

		public function get domain():IDomain
		{
			return _domain;
		}

		public function set domain(value:IDomain):void
		{
			_domain = value;
		}

	}
}
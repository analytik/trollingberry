package uglycode.trollingberry.character.variables
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.IStatProvider;

	public class Variables implements IStatProvider
	{
		public var varDb:Dictionary = new Dictionary();
		
		public static const BODY_ACTIVITY_INDEX:String = 'Body Activity Index';
		public static const SEXUAL_FRUSTRATION:String = 'Sexual frustration';
		public static const PEER_PRESSURE:String = 'Peer pressure';
		public static const SLEEP_DEPRIVATION:String = 'Sleep deprivation';
		public static const SOMNOLENCE:String = 'Somnolence';
		public static const EXHAUSTION:String = 'Exhaustion';
		public static const HYGIENE:String = 'Hygiene level';
		public static const CORTISOL:String = 'Stress level';
		public static const ADRENALINE:String = 'Adrenaline level';
		public static const TESTOSTERONE:String = 'Testosterone level';
		/**
		 * TODO v2:
		 * hunger
		 * stomach health (invisible) - produces diarrhoea
		 * adrenaline (excitement?)
		 * testosterone (anger?)
		 * cortisol (stress?)
		 */
		
		//and the magical pseudo-biorhythms
		public static const CREATIVITY:String = 'Creativity';
		public static const WILLPOWER:String = 'Willpower';
		public static const SOCIAL_INCLINATION:String = 'Desire to be social';
		public static const PHYSICAL_INCLINATION:String = 'Desire to move';
		
		public function Variables()
		{
			initVars();
			//tempRandomizeSkills();
		}
		
		
		
		private function initVars():void
		{
			varDb[BODY_ACTIVITY_INDEX] = new Variable(BODY_ACTIVITY_INDEX); //-20 = BAI 1.0 (lying down), 0 = 2.0, +20 = 3.0
			varDb[SEXUAL_FRUSTRATION] = new Variable(SEXUAL_FRUSTRATION);	//-20 = asexual, 0 = content, +20 = super horny
			varDb[PEER_PRESSURE] = new Variable(PEER_PRESSURE);				//-20 = independent, 0 = content, +20 = super insecure
			varDb[SLEEP_DEPRIVATION] = new Variable(SLEEP_DEPRIVATION);		//-20 = 10hrs sleep, -10 = 7.5hr sleep a day, 0 = +20 = less than 0.
			varDb[SOMNOLENCE] = new Variable(SOMNOLENCE);					//-20 = cool, +20 = fucking zombie
			varDb[EXHAUSTION] = new Variable(EXHAUSTION);					//-20 = full of energy, 0 = normal, +20 = dead tired
			varDb[EXHAUSTION].hourlyDecay = -1.20;
			varDb[HYGIENE] = new Variable(HYGIENE, 15);						//-20 = hobo, 0 = needs shower, 15 = clean, 20 = squeaky clean
			varDb[HYGIENE].hourlyDecay = 0.55;
			// LONG TERM TODO: MBTI affect these numbers
			varDb[CREATIVITY] = new Variable(CREATIVITY, 0, 1, -1, true, 120, 7);	//-20 = lame, +20 = super cool ideas
			varDb[WILLPOWER] = new Variable(WILLPOWER, 0, 1, -1, true, 90, 5);		//-20 = not doing shit, +20 - super active
			varDb[SOCIAL_INCLINATION] = new Variable(SOCIAL_INCLINATION, 0, 1, -1, true, 150, 7);		//-20 = asocial, +20 = needs partying now
			varDb[PHYSICAL_INCLINATION] = new Variable(PHYSICAL_INCLINATION, 0, 1, -1, true, 180, 7);	//-20 = sloth, +20 = needs running around
			//
			varDb[CORTISOL] = new Variable(CORTISOL);
			varDb[ADRENALINE] = new Variable(ADRENALINE);
			varDb[TESTOSTERONE] = new Variable(TESTOSTERONE);
		}
		
		
		public function getVerbalType():String {
			return 'variable';
		}
		public function getStats():Dictionary {
			return varDb;
		}
		public function isMehSkillDescriptionVisible():Boolean {
			return false;
		}
		public function isGaugeDisplayed():Boolean {
			return true;
		}
		public function getStatVisibilityMargin():Number {
			return -20.0;
		}
		public function getStatVisibilityDeadzone():Number {
			return 3;
		}
		public function getStatTitle():String {
			return 'Your current state:';
		}
		
		
		public function advanceTime(minutes:Number):void {
			for each (var v:Variable in varDb) 
			{
				//trace ('decaying variable ' + v.name);
				v.decay(minutes);
			}
		}
		
		private function tempRandomizeSkills():void {
			for (var s:String in varDb) {
				varDb[s].value = (Math.random()*40) - 20;
			}
		}
	}
}
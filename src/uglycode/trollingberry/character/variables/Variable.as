package uglycode.trollingberry.character.variables
{
	import uglycode.trollingberry.game.GameTime;
	import uglycode.trollingberry.game.IAffectable;
	import uglycode.trollingberry.game.INumericValueProvider;

	public class Variable implements IAffectable, INumericValueProvider //IDailyDecay
	{
		public var name:String;
		private var _value:Number;
		/**
		 * if someone has a special talent, increase this value. Special talents don't exist, so don't turn it up. Just lower it to annoy the fucker.
		 */
		public var factor:Number = 1.0;
		/**
		 * If someone is especially clumsy, just crank this shit up.
		 */
		public var factorNegative:Number = -1.0;
		
		public var isBiorhythmic:Boolean = false; //only for socialInclination, physicalInclination, Willpower and Creativity
		public var biorhythmAmplitude:Number = 7.0;	//radius (7), not diameter (14)
		public var biorhythmLength:Number = 120; //hours 
		private var _hourlyDecay:Number = 0;
		
		
		
		public function Variable(name:String, value:Number = 0, factor:Number = 1.0, factorNeg:Number = -1.0, isBiorhythmic:Boolean = false, biorhythmLength:Number = 120, biorhythmAmplitude:Number = 7)
		{
			this.name = name;
			this.value = value;
			this.factor = factor;
			this.factorNegative = factorNeg;
			this.isBiorhythmic = isBiorhythmic;
			this.biorhythmLength = biorhythmLength;
			this.biorhythmAmplitude = biorhythmAmplitude;
		}
		

		/**
		 * Enter positive number here to increase skill.
		 */
		public function increase(diff:Number):void {
			value = Math.min(value + (diff * factor), 20);
		}
		/**
		 * Don't forget to enter *positive* number here to decrease skill.
		 */
		public function decrease(diff:Number):void {
			value = Math.max(value + (diff * factorNegative), -20);
		}
		
		public function disable():void {}
		public function enable():void {}
		public function isEnabled():Boolean {
			return true;
		}

		public function get value():Number
		{
			return _value;
		}

		public function set value(value:Number):void
		{
			_value = value;
		}
		
		
		//////////////////////////////////////////////// whatever decay interface

		public function get hourlyDecay():Number
		{
			return _hourlyDecay;
		}
		
		/**
		 * Positive number will cause hourly decrease. Negative number will cause increase.
		 */
		public function set hourlyDecay(val:Number):void {
			_hourlyDecay = val;
		}
		
		public function decay(minutes:Number):void {
			//respects factors
			if (_hourlyDecay > 0) { 
				decrease(hourlyDecay * minutes/60);
			} 
			else if (_hourlyDecay < 0) {
				increase(-1 * hourlyDecay * minutes/60);
			}
			if (isBiorhythmic) {
				//calculated from GameTime.time - zero being zero, no offset
				// radian --- PI = (biorhythmLength*60)
				var oneMinuteInRadians:Number = Math.PI / (biorhythmLength*60);
				var currentStartingRadian:Number = GameTime.time * oneMinuteInRadians;
				var currentEndingRadian:Number = (GameTime.time + minutes) * oneMinuteInRadians;
				var diff:Number = ( Math.sin(currentEndingRadian) - Math.sin(currentStartingRadian) ) * biorhythmAmplitude;
				decrease(diff);
			}
		}
		

	}
}
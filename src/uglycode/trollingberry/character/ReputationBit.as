package uglycode.trollingberry.character
{
	import uglycode.trollingberry.game.IAffectable;
	import uglycode.trollingberry.game.INumericValueProvider;

	public class ReputationBit implements IAffectable, INumericValueProvider
	{
		private var name:String;
		private var levels:Array;
		private var _value:Number;
		//private var domainName:String;
		public  var factor:Number = 1.0;
		public  var factorNegative:Number = -1.0;
		private var parentReputation:Reputation;
		
		

		public function ReputationBit(ident:String, levels:Array, value:Number, parentRep:Reputation)
		{
			this.name = ident;
			this.levels = levels;
			this.value = value;
			//this.domainName = domainName;
			this.parentReputation = parentRep;
		}
		
		
		public function clone():ReputationBit {
			var cloned:ReputationBit = new ReputationBit(this.name, this.levels, this.value, this.parentReputation);
			cloned.factor = this.factor;
			cloned.factorNegative = this.factorNegative;
			return cloned;
		}
		
		
		/**
		 * Enter positive number here to increase skill.
		 */
		public function increase(diff:Number):void {
			value = Math.min(value + (diff * factor), 20);
		}
		
		/**
		 * Don't forget to enter *positive* number here to decrease skill.
		 */
		public function decrease(diff:Number):void {
			value = Math.max(value + (diff * factorNegative), -20);
		}
		
		public function disable():void {}
		public function enable():void {}
		public function isEnabled():Boolean {
			return true;
		}

		public function get value():Number
		{
			return _value;
		}

		public function set value(value:Number):void
		{
			_value = value;
		}
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
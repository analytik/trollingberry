package uglycode.trollingberry.character.disorders
{
	public class Disorder
	{
		public static const DISORDER_TYPE_MENTAL:String = 'mental'; //anxiety, depression, adhd
		public static const DISORDER_TYPE_VENEREAL:String = 'std'; //duhhh
		public static const DISORDER_TYPE_INFECTIOUS:String = 'infection'; //flu
		public static const DISORDER_TYPE_DISEASE:String = 'disease'; //cancer, diabetes
		public static const DISORDER_TYPE_SYMPTOM:String = 'symptom'; //headache, dizziness, nausea, diarrhoea
		
		//todo add specific disorder idents as constants
		
		public function Disorder()
		{
		}
	}
}
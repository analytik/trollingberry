package uglycode.trollingberry.character
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.game.IHuman;
	import uglycode.trollingberry.game.IHumanSpecific;
	import uglycode.trollingberry.game.IStatProvider;
	import uglycode.trollingberry.util.VerbalDescription;
	import uglycode.trollingberry.world.Topic;

	public class TopicKnowledge implements IStatProvider, IHumanSpecific
	{
		private var knowDb:Dictionary = new Dictionary;
		private var parentHuman:IHuman;
		
		
		
		public function TopicKnowledge(parent:IHuman)
		{
			this.initKnowledge(ServiceLocator.getInstance().topics.topicDb, true);
			//this.tempRandomizeKnowledge();
			/*
			todo you shit
			copy paste intrerface methods
			implement into Player Class
			*/
		}
		
		/*private function tempRandomizeKnowledge():void
		{
			for (var t:TopicKnowledgeBit in this.knowDb) {
				t.value = Math.Ran
			}
		}*/
		
		private function initKnowledge(topics:Dictionary, randomize:Boolean = false):void
		{
			for (var s:String in topics) 
			{
				//retard way, encapsulate in Reputation
				var repDescription:Array = VerbalDescription.getRandomKnowledgeLevels();
				this.knowDb[s] = new TopicKnowledgeBit(
					(topics[s] as Topic).name,
					repDescription,
					(randomize) ? (Math.random()*40) - 20 : 0
				);
			}
		}		
		
		public function getVerbalType():String {
			return 'knowledge';
		}
		public function getStats():Dictionary {
			return knowDb;
		}
		public function isMehSkillDescriptionVisible():Boolean {
			return false;
		}
		public function isGaugeDisplayed():Boolean {
			return false;
		}
		public function getStatVisibilityMargin():Number {
			return -20.0;
		}
		public function getStatVisibilityDeadzone():Number {
			return 3;
		}
		public function getStatTitle():String {
			return 'Knowledge';
		}
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
	}
}
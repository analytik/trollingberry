package uglycode.trollingberry.character
{
	import uglycode.trollingberry.game.IAffectable;
	import uglycode.trollingberry.game.INumericValueProvider;
	
	public class RelationshipBit implements IAffectable, INumericValueProvider
	{
		private var name:String;
		private var levels:Array;
		private var _value:Number;
		//private var domainName:String;
		public  var factor:Number = 1.0;
		public  var factorNegative:Number = -1.0;
		private var parentRelationship:Relationship;
		
		
		
		public function RelationshipBit()
		{
		}
		
		public function increase(diff:Number):void
		{
		}
		
		public function decrease(diff:Number):void
		{
		}
		
		public function disable():void
		{
		}
		
		public function enable():void
		{
		}
		
		public function isEnabled():Boolean
		{
			return false;
		}
		
		public function get value():Number
		{
			return 0;
		}
		
		public function set value(x:Number):void
		{
		}
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
	}
}
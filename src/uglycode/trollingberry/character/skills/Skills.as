package uglycode.trollingberry.character.skills
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.IStatProvider;

	public class Skills implements IStatProvider
	{
		public var skillDb:Dictionary;
		
		public static const WRITING:String = 'writing';
		public static const DRAWING:String = 'drawing';
		public static const PAINTING:String = 'painting';
		//handicraft, sewing, arts and craps
		public static const HANDICRAFT:String = 'handicraft';
		//mechanical skills, sowing, 
		public static const MECHANICAL:String = 'mechanical';
		//changing lightbulbs, plumbing, cleaning, woodwork
		public static const PRACTICAL:String = 'practical';
		// the spice of internet
		public static const PHOTOCHOPPING:String = 'photochopping';
		public static const ENGLISH:String = 'english';
		public static const LYING:String = 'lying';
		public static const HUMOUR:String = 'humour';
		public static const OBSERVATION:String = 'observation';
		public static const TROLLING:String = 'trolling';
		
		public function Skills()
		{
			initSkills();
			tempRandomizeSkills();
		}
		
		private function initSkills():void
		{
			skillDb = new Dictionary();
			skillDb[WRITING] = new Skill(WRITING, 'write', '');
			skillDb[DRAWING] = new Skill(DRAWING, 'draw', '');
			skillDb[PAINTING] = new Skill(PAINTING, 'paint', '');
			skillDb[HANDICRAFT] = new Skill(HANDICRAFT, 'create', '');
			skillDb[MECHANICAL] = new Skill(MECHANICAL, 'construct', '');
			skillDb[PRACTICAL] = new Skill(PRACTICAL, 'fix', '');
			skillDb[PHOTOCHOPPING] = new Skill(PHOTOCHOPPING, "'chop", '');
			skillDb[ENGLISH] = new Skill(ENGLISH, "write properly", '');
			skillDb[LYING] = new Skill(LYING, "lie", '');
			skillDb[HUMOUR] = new Skill(HUMOUR, "be funny", '');
			skillDb[OBSERVATION] = new Skill(OBSERVATION, 'notice details', 'overlook details');
			skillDb[TROLLING] = new Skill(TROLLING, 'annoy', '');
		}
		
		
		public function getVerbalType():String {
			return 'skill';
		}
		public function getStats():Dictionary {
			return skillDb;
		}
		public function isMehSkillDescriptionVisible():Boolean {
			return false;
		}
		public function isGaugeDisplayed():Boolean {
			return true;
		}
		public function getStatVisibilityMargin():Number {
			return -20.0;
		}
		public function getStatVisibilityDeadzone():Number {
			return 3;
		}
		public function getStatTitle():String {
			return 'Your skills:';
		}
		
		private function tempRandomizeSkills():void {
			for (var s:String in skillDb) {
				skillDb[s].value = (Math.random()*40) - 20;
			}
		}
	}
}
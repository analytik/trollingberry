package uglycode.trollingberry.character
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.game.IDomainSpecific;
	import uglycode.trollingberry.game.IStatProvider;
	
	
	/**
	 * This class will store RelationShip bits with every person
	 */
	public class Relationship implements IStatProvider, IDomainSpecific
	{
		public static const TYPE_EROS:String = 'romantic'; //ἔρως
		public static const TYPE_STORGE:String = 'affection'; //στοργή
		public static const TYPE_PHILIA:String = 'friendship'; //φιλία
		public static const TYPE_AGAPE:String = 'love'; //ἀγάπη
		//public static const TYPE_PORNEA:String = 'corruption'; //πορνεία - TODO move this into character/inclinations
		
		public static const STATUS_ACQ:String = 'acquaintance';
		public static const STATUS_FRIEND:String = 'friend';
		public static const STATUS_BFF:String = 'bff';
		public static const STATUS_DATE:String = 'date';
		public static const STATUS_LOVER:String = 'lover';
		public static const STATUS_SPOUSE:String = 'spouse';
		public static const STATUS_PARENT:String = 'parent';
		
		// primary status (bf, gf, parent, friend, acquaintance, etc)
		public var status:String;
		
		
		public function Relationship()
		{
		}
		
		public function getVerbalType():String
		{
			return null;
		}
		
		public function getStats():Dictionary
		{
			return null;
		}
		
		public function isMehSkillDescriptionVisible():Boolean
		{
			return false;
		}
		
		public function isGaugeDisplayed():Boolean
		{
			return false;
		}
		
		public function getStatVisibilityMargin():Number
		{
			return 0;
		}
		
		public function getStatVisibilityDeadzone():Number
		{
			return 0;
		}
		
		public function getStatTitle():String
		{
			return null;
		}
		
		public function get domain():IDomain
		{
			return null;
		}
		
		public function set domain(domain:IDomain):void
		{
		}
	}
}
package uglycode.trollingberry.character
{
	import uglycode.trollingberry.game.IAffectable;
	import uglycode.trollingberry.game.INumericValueProvider;

	public class TopicKnowledgeBit implements IAffectable, INumericValueProvider
	{
		public var name:String;
		public var levels:Array;
		private var _value:Number;
		public var factor:Number = 1.0;
		public var factorNegative:Number = -1.0;
		
		public function TopicKnowledgeBit(ident:String, levels:Array, value:Number)
		{
			this.name = ident;
			this.levels = levels;
			this.value = value;
		}
		
		
		
		/**
		 * Enter positive number here to increase skill.
		 */
		public function increase(diff:Number):void {
			value = Math.min(value + (diff * factor), 20);
		}
		
		/**
		 * Don't forget to enter *positive* number here to decrease skill.
		 */
		public function decrease(diff:Number):void {
			value = Math.max(value + (diff * factorNegative), -20);
		}
		
		public function disable():void {}
		public function enable():void {}
		public function isEnabled():Boolean {
			return true;
		}

		public function get value():Number
		{
			return _value;
		}

		public function set value(value:Number):void
		{
			_value = value;
		}

	}
}
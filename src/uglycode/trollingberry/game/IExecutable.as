package uglycode.trollingberry.game
{
	public interface IExecutable
	{
		function execute():void;
	}
}
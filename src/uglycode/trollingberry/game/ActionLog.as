package uglycode.trollingberry.game
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.consequence.Outcome;
	import uglycode.trollingberry.world.actions.Action;
	
	/**
	 * Stores all past actions
	 * Provides stats out of them (time wasted on games, fora, time wasted, people pissed off, etc)
	 * I dunno.
	 */
	public class ActionLog implements IStatProvider
	{
		public static const SPAN_TOTAL:String = 'total';
		public static const SPAN_TODAY:String = 'today';
		public static const SPAN_YDAY:String = 'yesterday';
		public static const SPAN_8H:String = '8h';
		public static const SPAN_12H:String = '12h';
		public static const SPAN_16H:String = '16h';
		public static const SPAN_20H:String = '20h';
		public static const SPAN_23H:String = '23h';
		public static const SPAN_24H:String = '24h';
		public static const SPAN_7DAYS:String = '7d';
		public static const SPAN_14DAYS:String = '14d';
		public static const SPAN_30DAYS:String = '30d';

		public var log:Vector.<ActionLogBit> = new Vector.<ActionLogBit>;
		//public var counts:Vector.<ActionCountBit> = new Vector.<ActionCountBit>;
		public var lastTime:Dictionary = new Dictionary;
		public var counts:Dictionary = new Dictionary;
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		public function ActionLog()
		{
			//
		}
		
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * TODO FOLLOWING SHIT:
		 */
		
		public function getVerbalType():String
		{
			return null;
		}
		
		public function getStats():Dictionary
		{
			return null;
		}
		
		public function isMehSkillDescriptionVisible():Boolean
		{
			return false;
		}
		
		public function isGaugeDisplayed():Boolean
		{
			return false;
		}
		
		public function getStatVisibilityMargin():Number
		{
			return 0;
		}
		
		public function getStatVisibilityDeadzone():Number
		{
			return 0;
		}
		
		public function getStatTitle():String
		{
			return null;
		}
		
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public function logOutcome(o:Outcome):void
		{
			var lastLog:ActionLogBit = new ActionLogBit().logDetails(o.action.ident, o.action.fullName, o.lastMessage, o.lastEffects); //time is logged automatically
			log.push(lastLog);
			lastTime[o.action.ident] = lastLog;
			
			//log count
			if (!counts.hasOwnProperty(o.action.ident)) {
				counts[o.action.ident] = new GenericNumberValueProvider();
			}
			(counts[o.action.ident] as GenericNumberValueProvider).value++;
		}
		
		
		
		public function getActionCount(actionIdent:String, timeScope:String = ActionLog.SPAN_TOTAL):GenericNumberValueProvider {
			if (timeScope == ActionLog.SPAN_TOTAL && this.counts.hasOwnProperty(actionIdent)) {
				return this.counts[actionIdent];
			}
			else if (timeScope == SPAN_24H) {
				return count(actionIdent, GameTime.oneDayBack);
			}
			else if (timeScope == SPAN_23H) {
				return count(actionIdent, GameTime.time - (60*23));
			}
			else if (timeScope == SPAN_20H) {
				return count(actionIdent, GameTime.time - (60*20));
			}
			else if (timeScope == SPAN_16H) {
				return count(actionIdent, GameTime.time - (60*16));
			}
			else if (timeScope == SPAN_12H) {
				return count(actionIdent, GameTime.time - (60*12));
			}
			else if (timeScope == SPAN_8H) {
				return count(actionIdent, GameTime.time - (60*8));
			}
			else if (timeScope == SPAN_TODAY) {
				return count(actionIdent, GameTime.lastMidnight);
			}
			else if (timeScope == SPAN_YDAY) {
				return count(actionIdent, GameTime.previousLastMidnight);
			}
			else if (timeScope == SPAN_7DAYS) {
				return count(actionIdent, GameTime.getDaysBack(7));
			}
			else if (timeScope == SPAN_14DAYS) {
				return count(actionIdent, GameTime.getDaysBack(14));
			}
			else if (timeScope == SPAN_30DAYS) {
				return count(actionIdent, GameTime.getDaysBack(30));
			}
			return new GenericNumberValueProvider;
		}
		
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		private function count(actionIdent:String, startingTime:uint):GenericNumberValueProvider
		{
			var ac:GenericNumberValueProvider = new GenericNumberValueProvider;
			for each (var logitem:ActionLogBit in log) 
			{
				//trace ('item name ' + logitem.ident + ' vs ' + actionIdent + ' -- ' + startingTime + ' / ' + logitem.value);
				if (logitem.ident == actionIdent && logitem.value >= startingTime) {
					ac.value++;
				}
			}
			return ac;
		}
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
		
		
		
	}
}
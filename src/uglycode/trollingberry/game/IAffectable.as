package uglycode.trollingberry.game
{
	public interface IAffectable
	{
		function increase(diff:Number):void;
		function decrease(diff:Number):void;
		function disable():void;
		function enable():void;
		function isEnabled():Boolean;
		
	}
}
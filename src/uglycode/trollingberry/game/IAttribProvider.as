package uglycode.trollingberry.game
{
	import flash.utils.Dictionary;

	public interface IAttribProvider
	{
		function hasAttribIndex(key:String):Boolean;
		function hasStringAttribIndex(key:String):Boolean;
		function hasNumericAttribIndex(key:String):Boolean;
		function hasComplexAttribIndex(key:String):Boolean;
		
		function getStringAttrib(key:String):String;
		function getNumericAttrib(key:String):Number;
		function getComplexAttrib(key:String):Object;
		
		function setStringAttrib(key:String, value:String):void;
		function setNumericAttrib(key:String, value:Number):void;
		function setComplexAttrib(key:String, value:Object):void;
		
		function get allStringAttribs():Dictionary;
		function get allNumericAttribs():Dictionary;
		function get allComplexAttribs():Dictionary;

		
		
		/* reference implementation
		private var _attribsString:Dictionary;
		private var _attribsNumeric:Dictionary;
		private var _attribsComplex:Dictionary;
		
		public function hasAttribIndex(key:String):Boolean {
		return _attribsString.hasOwnProperty(key) || _attribsNumeric.hasOwnProperty(key) || _attribsComplex.hasOwnProperty(key);
		}
		public function hasStringAttribIndex(key:String):Boolean {
		return _attribsString.hasOwnProperty(key);
		}
		public function hasNumericAttribIndex(key:String):Boolean {
		return _attribsNumeric.hasOwnProperty(key);
		}
		public function hasComplexAttribIndex(key:String):Boolean {
		return _attribsComplex.hasOwnProperty(key);
		}
		
		
		public function setStringAttrib(key:String, value:String):void {this._attribsString[key] = value;}
		public function setNumericAttrib(key:String, value:Number):void {this._attribsNumeric[key] = value;}
		public function setComplexAttrib(key:String, value:Object):void {this._attribsComplex[key] = value;}
		
		
		public function getStringAttrib(key:String):String {
		return hasStringAttribIndex(key) ? _attribsString[key] : null;
		}
		public function getNumericAttrib(key:String):Number {
		return hasNumericAttribIndex(key) ? _attribsNumeric[key] : null;
		}
		public function getComplexAttrib(key:String):Object {
		return hasComplexAttribIndex(key) ? _attribsComplex[key] : null;
		}
		
		
		public function get allStringAttribs():Dictionary {
		return this._attribsString;
		}
		public function get allNumericAttribs():Dictionary {
		return this._attribsNumeric;
		}
		public function get allComplexAttribs():Dictionary {
		return this._attribsComplex;
		}
		*/
		
	}
}
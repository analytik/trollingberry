package uglycode.trollingberry.game
{
	/**
	 * this is quite awkward.
	 */
	public class GenericNumberValueProvider implements INumericValueProvider
	{
		protected var _value:Number = 0;
		
		
		
		public function GenericNumberValueProvider()
		{
		}
		
		public function get value():Number
		{
			return _value;
		}
		
		/**
		 * count of this action happening in the current playthrough
		 */
		public function set value(x:Number):void
		{
			_value = x;
		}
	}
}
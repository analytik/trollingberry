package uglycode.trollingberry.game
{
	public interface ITimeAdvancable
	{
		function advanceTime(minutes:Number):void;
	}
}
package uglycode.trollingberry.game
{
	import flash.utils.Dictionary;

	public interface IStatProvider
	{
		function getVerbalType():String;
		function getStats():Dictionary;
		function isMehSkillDescriptionVisible():Boolean;
		function isGaugeDisplayed():Boolean;
		function getStatVisibilityMargin():Number;
		/**
		 * Instructs to now publicly show this stat if it's in range -x < stat < +x
		 */
		function getStatVisibilityDeadzone():Number;
		function getStatTitle():String;
	}
}
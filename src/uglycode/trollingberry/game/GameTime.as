package uglycode.trollingberry.game
{
	import flash.utils.getTimer;

	/**
	 * Game time is represented in minutes, starting with Day 0, minute 0
	 */
	public class GameTime
	{
		private static var _time:uint = 0;
		
		private static var _scheduledTimes:Vector.<uint> = new Vector.<uint>;
		private static var _scheduledActions:Vector.<IExecutable> = new Vector.<IExecutable>;
		private static var timeAdvancers:Vector.<ITimeAdvancable> = new Vector.<ITimeAdvancable>;
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function GameTime()
		{
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * Current day of the game (not week!)
		 */
		public static function get day():uint {
			return Math.ceil(_time / dayLength);
		}
		
		
		/**
		 * Hour of the day
		 */
		public static function get hour():uint {
			return Math.floor((_time % dayLength) / 60);
		}
		
		
		/**
		 * Minute of the hour
		 */
		public static function get minute():uint {
			return _time % 60;
		}
		
		
		/**
		 * Number of week since game start
		 */
		public static function get week():uint {
			return Math.ceil(_time / (dayLength*7)); //10080
		}

		
		public static function get time():uint
		{
			return _time;
		}
		
		
		public static function set time(value:uint):void
		{
			runScheduledTasks();
			if (value < _time) {
				throw new Error('Cannot move time back, innit.');
			}
			_time = value;
		}
		
		
		public static function get lastMidnight():uint {
			return time - (hour * 60) - minute;
		}
		
		public static function get previousLastMidnight():uint {
			return Math.max(0, time - (hour * 60) - minute - dayLength);
		}
		
		
		public static function get oneDayBack():uint {
			return Math.max(0, time - dayLength);
		}
		
		public static function getDaysBack(daysBack:uint):uint {
			return Math.max(0, time - (daysBack * dayLength));
		}
		
		
		public static function get dayLength():uint {
			return 1440;
		}
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public static function scheduleMinutesFromNow(minutesInFuture:Number, action:IExecutable):void {
			//"index" will be the absolute minute when it's supposed to be run
			_scheduledTimes.push(minutesInFuture + _time);
			//value will be the Command, of course
			_scheduledActions.push(action);
		}

		
		
		public static function advance(value:uint):void {
			time += value;
			for each (var obj:ITimeAdvancable in timeAdvancers) 
			{
				obj.advanceTime(value);
			}
		}
		
		
		
		public static function registerForTimePassage(stuff:ITimeAdvancable):void {
			timeAdvancers.push(stuff);
		}
		

		
		public static function reset():void
		{
			_time = 0;
		}
		
		
		public static function readable(value:Number):String
		{
			var ret:String;
			var oldTime:uint = _time;
			_time = value;
			ret = 'Day ' + day + ', ' + ((hour > 9) ? hour : '0' + hour) + ':' + ((minute > 9) ? minute : '0' + minute);
			_time = oldTime;
			return ret;
		}
		
		
		
		/**
		 * For GameTime condition, possible values 'week', 'month', 'day' or 'dayofweek'
		 */
		
		public static function getCVE(property:String):INumericValueProvider
		{
			var dumb:GenericNumberValueProvider = new GenericNumberValueProvider;
			if (property == 'week') {
				dumb.value = week;
			}
			else if (property == 'month') {
				dumb.value = (day % 30);
			}
			else if (property == 'day') {
				dumb.value = day;
			}
			else if (property == 'dayofweek') {
				dumb.value = (day % 7) + 1;
			}
			else {
				throw new Error('derrrrpppp?');
			}
			return dumb;
		}

		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		
		
		private static function runScheduledTasks():void
		{
			var newArray:Array = [];
			var deleteIndexes:Array = [];
			for (var i:uint in _scheduledTimes) 
			{
				if (_scheduledTimes[i] <= time) {
					newArray[_scheduledTimes[i]] = _scheduledActions[i];
					deleteIndexes.push(i);
				}
			}
			if (newArray.length > 0) {
				newArray.sort(Array.NUMERIC);
				trace ('running ' + newArray.length + ' scheduled tasks.');
				for (i in newArray)
				{
					newArray[i].execute();
				}
			}
			//delete them from last to the first because derp
			deleteIndexes.sort(Array.NUMERIC | Array.DESCENDING);
			for (i in deleteIndexes) {
				_scheduledActions.splice(i, 1);
				_scheduledTimes.splice(i, 1);
			}
		}
		
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
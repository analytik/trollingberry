package uglycode.trollingberry.game
{
	public interface IDomainSpecific
	{
		function get domain():IDomain;
		function set domain(domain:IDomain):void;
	}
}
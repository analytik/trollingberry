package uglycode.trollingberry.game
{
	import uglycode.trollingberry.character.Reputation;

	public interface IDomain
	{
	
		/**
		 * "Domain" is a context for interactions and reputation for
		 * - activities (online and RL - hobbies, group hobbies and sports, communities)
		 * - people (family, spouse, friends, online NPCs)
		 * - life roles (school, work)
		 */
		
		function get name():String;
		function get reputation():Reputation;
	}
}
package uglycode.trollingberry.game
{
	import uglycode.trollingberry.game.consequence.Effects;

	public class ActionLogBit implements INumericValueProvider
	{
		public var ident:String;
		public var effects:Effects;
		
		protected var _value:Number = 0;
		public var message:String;
		public var fullName:String;
		
		
		
		public function ActionLogBit()
		{
			this._value = GameTime.time;
		}
		
		
		
		public function logDetails(ident:String, readableIdent:String, message:String, effects:Effects):ActionLogBit {
			this.ident = ident;
			this.fullName = readableIdent;
			this.effects = effects;
			this.message = message;
			//fluid interface, bitches
			return this;
		}
		
		
		/**
		 * Retarded, but in this case just denotes the time it was executed 
		 */
		public function get value():Number
		{
			return _value;
		}
		
		
		
		/**
		 * last game minute of this action happening
		 */
		public function set value(x:Number):void
		{
			_value = x;
		}
	}
}
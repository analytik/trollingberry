package uglycode.trollingberry.game.consequence
{
	public interface ITag
	{
		function get name():String;
		function get conditions():Vector.<Condition>;
		function get effects():Effects;
	}
}
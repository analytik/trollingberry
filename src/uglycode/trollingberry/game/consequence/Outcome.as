package uglycode.trollingberry.game.consequence
{
	import uglycode.trollingberry.game.GameTime;
	import uglycode.trollingberry.world.actions.Action;
	
	public class Outcome
	{
		public var action:Action;
		protected var chance:Number = 1.0;
		protected var successChance:Number = 1.0;
		public var conditions:Conditions = new Conditions();
		public var effects:Effects = new Effects();
		public var lastEffects:Effects = null;
		/**
		 * This doesn't work anyway
		 */
		public var multiOutcome:Boolean = false;
		public var priority:uint = 5;
		public var ident:String;
		public var message:String;			//e.g. You've posted on forum and were completely ignored
		public var failureMessage:String;	//message that will be presented if conditions are not met, but chance criterion is met.
		public var lastMessage:String;		//after execute, this will store failure/sucess message
		public var time:uint;
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function Outcome(parentAction:Action, message:String = '')
		{
			this.action = parentAction;
			this.message = message;
		}
		
		
		
		public static function create(parentAction:Action, ident:String, message:String, timeTaken:uint = 15,
			conditions:Conditions = null, effects:Effects = null, 
			chanceOfHappening:Number = 1.0, priority:uint = 5, chanceOfSuccess:Number = 1.0, 
			multiOutcome:Boolean = false, failureMsg:String = ''):Outcome
		{
			var o:Outcome = new Outcome(parentAction, message);
			o.ident = ident;
			if (chanceOfHappening > 1.0 || chanceOfHappening < 0.001) {
				throw new Error('range, fuckass');
			}
			o.chance = chanceOfHappening;
			if (chanceOfSuccess > 1.0 || chanceOfSuccess < 0.001) {
				throw new Error('range, fuckass');
			}
			o.successChance = chanceOfSuccess;
			if (priority > 10 || priority < 0) {
				throw new Error('range, fuckass');
			}
			o.priority = priority;
			o.time = timeTaken;
			if (conditions) o.conditions = conditions;
			if (effects) o.effects = effects;
			o.multiOutcome = multiOutcome;
			o.failureMessage = failureMsg;
			
			return o;
		}
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		/**
		 * =================================================================== init ===========================================================================
		 */
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		public function evaluate():Boolean {
			return this.conditions.evaluate() && (Math.random() <= this.chance);
		}
		
		
		public function evaluateSuccess():Boolean {
			return this.conditions.evaluate() && (Math.random() <= this.successChance);
		}

		
		
		public function execute():void
		{
			if (this.evaluateSuccess()) {
				trace ('Outcome.execute - ident ' + this.ident + ", result: \n\t" + this.message );
				lastMessage = this.message;
			}
			else {
				trace ('Outcome.execute YOU FAIL - ident ' + this.ident + ", result: \n\t" + this.failureMessage );
				lastMessage = this.failureMessage;
			}
			//apply Effects
			effects.execute();
			
			//move time ahead
			GameTime.advance(time);
		}
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
		

	}
}
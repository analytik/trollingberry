package uglycode.trollingberry.game.consequence
{
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.game.IAffectable;
	import uglycode.trollingberry.game.IAttribProvider;
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.game.IExecutable;

	public class Command implements IExecutable
	{
		public static const TYPE_SKILL:String = 'skill';
		public static const TYPE_STAT:String = 'stat';
		public static const TYPE_VARIABLE:String = 'variable';
		public static const TYPE_KNOWLEDGE:String = 'topicKnowledge';
		//public static const TYPE_PLAYER_RELATIONSHIP:String = 'relationship';
		public static const TYPE_REPUTATION:String = 'reputation';
		public static const TYPE_TOPIC_REPUTATION:String = 'topicReputation';
		public static const TYPE_ATTRIBUTE_PLAYER:String = 'attribPlayer';
		public static const TYPE_ATTRIBUTE_ACTIVITY:String = 'attribActivity';
		
		public static var valueNouns:Array = [
			TYPE_SKILL,
			TYPE_STAT,
			TYPE_VARIABLE,
			TYPE_KNOWLEDGE,
			];
		
		public static var domainNouns:Array = [
			//'relationship',
			TYPE_REPUTATION,
			TYPE_TOPIC_REPUTATION
			];
		
		//FUTURE TODO V2 - shit for manipulating prop->value condition providing objects
		// like Player, Traits, Disorders
		
		public static var verbs:Array = [
			'set',
			'inc',
			'dec'
			//todo: 'reset' - change all interfaces etc
			//todo maybe: 'temporarily boost coefficient'
			//todo: enable/disable
			];
		
		protected var noun:String; //obv
		protected var verb:String; //obv
		protected var bit:String;  //speicific Skill, Variable, etc
		protected var domain:IDomain;
		protected var value:Number;
		protected var anyValue:*;
		protected var key:String;
		
		
			
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function Command() {
			//meh, use static factory methods
		}
		
		
		public static function createValueCommand(noun:String, verb:String, bit:String, value:Number, parentDomain = null):Command
		{
			var c:Command = new Command;
			c.noun = noun; //todo validate
			c.verb = verb; // ditto
			c.bit = bit;
			c.value = value;
			c.domain = parentDomain;
			return c;
		}
		
		
		public static function createAttribCommand(noun:String, bit:String, key:String, value:*):Command {
			var c:Command = new Command;
			c.noun = noun;
			c.verb = 'set';
			c.key = key;
			c.bit = bit;
			c.anyValue = value;
			return c;
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public function execute():void {
			if (verb == 'set' && key) {
				var attribTarget:IAttribProvider = this.findAttribTarget(noun, bit);
				if (!attribTarget) {
					throw new Error ("Check your providing-invalid-noun/bit-combination privilege.");
				}
				if (value is String) {
					attribTarget.setStringAttrib(key, anyValue);
				}
				else if (value is Number) {
					attribTarget.setNumericAttrib(key, anyValue);
				}
				else if (value is Object) {
					attribTarget.setComplexAttrib(key, anyValue);
				}
			}
			else {
				//find target
				var targetNV:IAffectable = this.findTarget(noun, bit, domain);
				//increment/decrement/whatever we please
				if (!targetNV) {
					throw new Error ("The princess is in another castle.");
				}
				if (verb == 'inc') {
					targetNV.increase(value);
				}
				else if (verb == 'dec') {
					targetNV.decrease(value);
				}
			}
			//other actions here
		}
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		protected function findTarget(type:String, bit:String, parentDomain:IDomain = null):IAffectable
		{
			var p:Player = ServiceLocator.getInstance().player;
			if (type == TYPE_SKILL) {
				return p.skills.skillDb[bit];
			}
			else if (type == TYPE_STAT) {
				return p.stats.getStats()[bit];
			}
			else if (type == TYPE_VARIABLE) {
				return p.variables.varDb[bit];
			}
			else if (type == TYPE_KNOWLEDGE) {
				return p.knowledge.getStats()[bit];
			}
			else if (type == TYPE_REPUTATION) {
				if (parentDomain === null) {
					throw new Error ('Reputation requires a domain! Give me an Activity or something.');
				}
				return parentDomain.reputation.repDb[bit];
			}
			else if (type == TYPE_TOPIC_REPUTATION) {
				if (parentDomain === null) {
					throw new Error ('Reputation requires a domain! Give me an Activity or something.');
				}
				return parentDomain.reputation.repDb[bit];
			}

			return null;
		}
		
		protected function findAttribTarget(type:String, bit:String):IAttribProvider
		{
			//attrib section
			if (type == TYPE_ATTRIBUTE_PLAYER) {
				if (bit == '' || bit == 'player') {
					return ServiceLocator.getInstance().player;
				} //else get something else specific to player in the future
			}
			else if (type == TYPE_ATTRIBUTE_ACTIVITY) {
				return ServiceLocator.getInstance().player.activities.findActivity(bit);
			}
			return null;
		}
		
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
	}
}
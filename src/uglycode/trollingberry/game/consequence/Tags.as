package uglycode.trollingberry.game.consequence
{
	public class Tags
	{
		public static const GLOBAL:String = 'GLOBAL';
		//public static const GLOBAL:String = 'GLOBAL';
		
		//user-defined tags (defined in XML of Activity/story/room/whatever definitons)
		public static var customTags:Vector.<String>;
		
		public static var tagDb:Vector.<ActionTag>;
		
		public function Tags()
		{
		}
		
		public function init():void {
			//set up all hard coded tags - GLOBAL, ONLINE, DEPRESSING, LEARNING, ENCOURAGING, BORING, STIMULATING_PHYSICALLY, etc
		}
	}
}
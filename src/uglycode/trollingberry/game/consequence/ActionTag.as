package uglycode.trollingberry.game.consequence
{

	public class ActionTag implements ITag
	{
		private var _name:String;
		/**
		 * TODO: these conditions will be checked first in the ITaggable object, and then &&'ed with own Conditions
		 */
		private var _conditions:Vector.<Condition> = new Vector.<Condition>;
		/**
		 * TODO: these Effects will be merged into ANY Outcome that might happen.
		 */
		private var _effects:Effects;
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function ActionTag()
		{
		}
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */

		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

		public function get conditions():Vector.<Condition>
		{
			return _conditions;
		}

		public function set conditions(value:Vector.<Condition>):void
		{
			_conditions = value;
		}

		public function get effects():Effects
		{
			return _effects;
		}

		public function set effects(value:Effects):void
		{
			_effects = value;
		}

		/**
		 * =================================================================== init ===========================================================================
		 */
		/**
		 * =================================================================== public methods =================================================================
		 */
		/**
		 * =================================================================== handlers =======================================================================
		 */
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
	}
}
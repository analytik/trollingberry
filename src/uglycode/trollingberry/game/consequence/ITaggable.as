package uglycode.trollingberry.game.consequence
{
	/**
	 * Interface that describes communication from constructing object (Activity) to child (Action)
	 * e.g. x = new Action; x.addTag(Tags.DEPRESSING, Tags.ONLINE) 
	 */
	public interface ITaggable
	{
		function addTag(ident:String):void;
		
		//function mergeEffects?
		//function checkTagConditions?
	}
}
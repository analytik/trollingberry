package uglycode.trollingberry.game.consequence
{
	import uglycode.trollingberry.game.GameTime;
	import uglycode.trollingberry.game.GenericNumberValueProvider;
	import uglycode.trollingberry.game.INumericValueProvider;

	
	
	public class ConditionElement
	{
		public static const FUNCTION_GT:String = 'gt';
		public static const FUNCTION_LT:String = 'lt';
		public static const FUNCTION_MIN:String = 'min';
		public static const FUNCTION_MAX:String = 'max';
		public static const FUNCTION_EQUALS:String = 'eq';
		public static const FUNCTION_NOT_EQUAL:String = 'neq';
		public static const FUNCTION_MOD_ZERO:String = 'mod';
		//public static const FUNCTION_MAX_DIFFERENCE_SECONDS:String = 'maxDiffSec';
		//public static const FUNCTION_MIN_DIFFERENCE_SECONDS:String = 'minDiffSec';
		public static const FUNCTION_MAX_DIFFERENCE_MINUTES:String = 'maxDiffMin';
		public static const FUNCTION_MIN_DIFFERENCE_MINUTES:String = 'minDiffMin';

		public var targetNVP:INumericValueProvider;
		
		public var method:String;
		public var target:*;
		public var comparisonValue:*;
		
		public function ConditionElement(target:* = null, method:String = null, value:* = null)
		{
			if (target is INumericValueProvider) {
				this.targetNVP = target;
				this.target = targetNVP.value;
			}
			else {
				this.target = target;
			}
			if (!target) {
				var breakpoint:String = 'isHere';
			}
			this.method = method;
			this.comparisonValue = value;
		}
		
		
		public function evaluate():Boolean {
			if (comparisonValue is Number) {
				return this.evaluateNumber();
			}
			else if (comparisonValue is String) {
				return this.evaluateString();
			}
			else if (comparisonValue is Object) {
				return this.evaluateObject();
			}
			return false;
		}
		
		
		
		protected function evaluateString():Boolean {
			if (method == FUNCTION_EQUALS) {
				if (target == comparisonValue) {
					return true;
				}
			}
			return false;
		}
		
		
		
		protected function evaluateObject():Boolean {
			//todo methods like unique keys equals, sorted equals, length same, etc 
			if (method == FUNCTION_EQUALS) {
				if (target == comparisonValue) {
					return true;
				}
			}
			return false;
		}
		
		
		
		protected function evaluateNumber():Boolean {
			if (!target && target !== 0) {
				throw new Error('no Target for comparison');
			}
			//var d:Date = new Date();
			if (method == FUNCTION_MIN) {
				if (target >= comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_MAX) {
				if (target <= comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_GT) {
				if (target > comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_LT) {
				if (target < comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_EQUALS) {
				if (target == comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_NOT_EQUAL) {
				if (target != comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_MOD_ZERO) {
				if ((target % comparisonValue) == 0) {
					return true;
				}
			}
			//calculating real world time for whatever reason - probably never used
			// misleading - after game load would provide incorrent data
			/*
			else if (method == FUNCTION_MAX_DIFFERENCE_SECONDS) {
				if ((d.getTime() / 1000) - target.value <= comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_MIN_DIFFERENCE_SECONDS) {
				if ((d.getTime() / 1000) - target.value >= comparisonValue) {
					return true;
				}
			}
			*/
			//calculating game time
			else if (method == FUNCTION_MAX_DIFFERENCE_MINUTES) {
				if (GameTime.time - target <= comparisonValue) {
					return true;
				}
			}
			else if (method == FUNCTION_MIN_DIFFERENCE_MINUTES) {
				if (GameTime.time - target <= comparisonValue) {
					return true;
				}
			}
			return false;
		}
	}
}
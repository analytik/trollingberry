package uglycode.trollingberry.game.consequence
{
	import uglycode.trollingberry.game.GameTime;

	public class Effects
	{
		public var immediate:Vector.<Command> = new Vector.<Command>;
		/**
		 * First 24 hours, zeroth index is one hour from now
		 */
		public var shortTerm:Vector.<Vector.<Command>> = new Vector.<Vector.<Command>>(24);
		/**
		 * First week in 6hr increments, zeroth index is 6hrs from now
		 */
		public var midTerm:Vector.<Vector.<Command>> = new Vector.<Vector.<Command>>(42);
		/**
		 * Daily effects, 0th index is 24hrs from now
		 */
		public var longTerm:Vector.<Vector.<Command>> = new Vector.<Vector.<Command>>(60);
		public function Effects()
		{
			//immediate
			//short term - 24 hours
			//mid term - 7 days every 6 hours
			//long term - 60 days every 1 day
		}
		
		public function execute():void
		{
			var i:uint;
			for each (var c:Command in immediate) 
			{
				c.execute();
			}
			
			for each (var vec:Vector.<Command> in shortTerm) 
			{
				for (i in vec) {
					GameTime.scheduleMinutesFromNow((vec[i]+1) * 60, vec[i]);
				}
			}
			for each (vec in midTerm) 
			{
				for (i in vec) {
					GameTime.scheduleMinutesFromNow((vec[i]+1) * 360, vec[i]);
				}
			}
			for each (vec in longTerm) 
			{
				for (i in vec) {
					GameTime.scheduleMinutesFromNow((vec[i]+1) * 1440, vec[i]);
				}
			}
			
		}
	}
}
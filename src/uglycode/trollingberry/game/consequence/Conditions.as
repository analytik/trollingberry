package uglycode.trollingberry.game.consequence
{
	import uglycode.trollingberry.game.ActionLog;
	import uglycode.trollingberry.game.IAttribProvider;
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.game.INumericValueProvider;

	/**
	 * A class that figures out whether conditions are fulfilled or not
	 * 
	 */
	public class Conditions
	{
		public var conditions:Vector.<Condition> = new Vector.<Condition>;
		
		public function Conditions()
		{
		}
		
		
		

		
		
		

		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		public function evaluate():Boolean {
			if (conditions.length == 0) {
				//if we have no conditions then we've automatically fulfilled them!
				return true;
			}
			var levels:Array = this.initLevelArray();
			var result:Boolean = false;
			for each (var c:Condition in conditions) 
			{
				levels[c.level] = levels[c.level] && c.evaluate();
			}
			for each (var partialResult:Boolean in levels) {
				result = result || partialResult;
			}
			return result;
		}
		
		
		/**
		 * =================================================================== condition adding shortcuts =====================================================
		 */
		
		
		public function addLastActionTimeCondition(targetActionIdent:String, method:String, value:Number):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_PLAYER_LAST_ACTION, targetActionIdent);
			//var a:Action = ServiceLocator.getInstance().player.activities.findAction(action);
			//if (!a) return;
			c.lastActionTime.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addActionCountCondition(targetActionIdent:String, method:String, value:Number, timeRange:String = ActionLog.SPAN_TOTAL, level:uint = 5):void {
			var c:Condition = new Condition;
			c.level = level;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_PLAYER_ACTION_COUNT, targetActionIdent, timeRange);
			c.actionCount.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addSkillCondition(targetBit:String, method:String, value:Number):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_PLAYER_SKILL, targetBit);
			c.skills.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addStatCondition(targetBit:String, method:String, value:Number):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_PLAYER_STAT, targetBit);
			c.stats.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addVariableCondition(targetBit:String, method:String, value:Number):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_PLAYER_VARIABLE, targetBit);
			c.variables.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addKnowledgeCondition(targetBit:String, method:String, value:Number):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_PLAYER_KNOWLEDGE, targetBit);
			c.knowledge.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addReputationCondition(targetBit:String, method:String, value:Number, repDomain:IDomain):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_CURRENT_ACTIVITY_PLAYER_REPUTATION, targetBit, null, repDomain);
			c.reputation.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addTopicReputationCondition(targetBit:String, method:String, value:Number, repDomain:IDomain):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_CURRENT_ACTIVITY_PLAYER_TOPIC_REPUTATION, targetBit, null, repDomain);
			c.topicReputation.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		/**
		 * GameTime condition, e.g. Week >= 3, Day = 7 DayOfWeek <= 4; possible values 'week', 'month', 'day' or 'dayofweek'
		 */
		public function addTimeCondition(targetBit:String, method:String, value:Number):void {
			var c:Condition = new Condition;
			var target:INumericValueProvider = c.findTarget(Condition.TYPE_TIME, targetBit);
			c.topicReputation.push(new ConditionElement(target, method, value));
			conditions.push(c);
		}
		
		
		public function addNumericAttribCondition(targetType:String, targetBit:String, key:String, method:String, comparisionValue:Number):void {
			var c:Condition = new Condition;
			var target:IAttribProvider = c.findAttribTarget(targetType, targetBit);
			c.topicReputation.push(new ConditionElement(target.getNumericAttrib(key), method, comparisionValue));
			conditions.push(c);
		}
		
		public function addStringAttribCondition(targetType:String, targetBit:String, key:String, method:String, comparisionValue:String):void {
			var c:Condition = new Condition;
			var target:IAttribProvider = c.findAttribTarget(targetType, targetBit);
			c.topicReputation.push(new ConditionElement(target.getNumericAttrib(key), method, comparisionValue));
			conditions.push(c);
		}
		
		/**
		 * huh... try to parse texts like
		 * - inventory has medkit
		 * - reputation knowledge >= 10, {IDomain obj}
		 * - actionCount tumblr::confirm-registration-email == 1
		 * - skill drawing < -5
		 * - time dayofweek != 2
		 */
		public function parseCondition(string:String, repDomain:IDomain = null):void {
			//
		}
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		private function initLevelArray():Array
		{
			var a:Array = [];
			for each (var c:Condition in conditions) {
				a[c.level] = true;
			}
			return a;
		}
		
		
		
	}
}
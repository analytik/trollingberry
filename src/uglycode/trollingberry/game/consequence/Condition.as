package uglycode.trollingberry.game.consequence
{
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.game.ActionLogBit;
	import uglycode.trollingberry.game.GameTime;
	import uglycode.trollingberry.game.IAttribProvider;
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.game.INumericValueProvider;

	public class Condition
	{
		public static const TYPE_PLAYER_LAST_ACTION:String = 'lastAction';
		public static const TYPE_PLAYER_ACTION_COUNT:String = 'actionCount';
		
		public static const TYPE_PLAYER_SKILL:String = 'skill';
		public static const TYPE_PLAYER_STAT:String = 'stat';
		public static const TYPE_PLAYER_VARIABLE:String = 'variable';
		public static const TYPE_PLAYER_KNOWLEDGE:String = 'knowledge';
		public static const TYPE_CURRENT_ACTIVITY_PLAYER_REPUTATION:String = 'reputation';
		public static const TYPE_CURRENT_ACTIVITY_PLAYER_TOPIC_REPUTATION:String = 'topicReputation';
		
		public static const TYPE_ATTRIBUTE_ACTIVITY:String = 'attribActivity';
		public static const TYPE_ATTRIBUTE_PLAYER:String = 'attribPlayer';
		
		public static const TYPE_TIME:String = 'time';
		/**
		 * Conditions on the same level are ANDed, their result is OR'd with other levels one by one
		 */
		public var level:uint = 5;
		
		/**
		 * activity_id:string => ConditionElementObject
		 */
		public var lastActionTime:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var actionCount:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		
		public var skills:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var stats:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var variables:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var reputation:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var knowledge:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var topicReputation:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var time:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		public var attribConditions:Vector.<ConditionElement> = new Vector.<ConditionElement>;
		//
		
		//THESE ALL WILL NEED TO IMPLEMENT ADVANCED getConditionablePropertyList / getProperty INTERFACE
		//FUTURE disorders yes no
		//FUTURE traits
		//FUTURE items, items property
		//FUTURE life role property
		//FUTURE relationship status with... <--- could be done with simple string:value ConditionValueElement (friendship: -20 to +20 etc)
		//FUTURE friend-targetted condition
		//FUTURE project status
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		public function Condition()
		{
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		/**
		 * =================================================================== init ===========================================================================
		 */
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public function evaluate():Boolean {
			//trace ('eval cond');
			if (
				lastActionTime.length == 0 &&
				actionCount.length == 0 &&
				skills.length == 0 &&
				stats.length == 0 &&
				variables.length == 0 &&
				reputation.length == 0 &&
				knowledge.length == 0 &&
				topicReputation.length == 0 &&
				time.length == 0
				) {
				return true;
			}
			var partialResult:Array = new Array;
			var nce:ConditionElement;
			if (lastActionTime.length > 0) {
				partialResult[0] = true;
				for each (nce in lastActionTime) {
					partialResult[0] &&= nce.evaluate();
				}
			}
			if (actionCount.length > 0) {
				partialResult[1] = true;
				for each (nce in actionCount) {
					partialResult[1] &&= nce.evaluate();
				}
			}
			if (skills.length > 0) {
				partialResult[2] = true;
				for each (nce in skills) {
					partialResult[2] &&= nce.evaluate();
				}
			}
			if (stats.length > 0) {
				partialResult[3] = true;
				for each (nce in stats) {
					partialResult[3] &&= nce.evaluate();
				}
			}
			if (variables.length > 0) {
				partialResult[4] = true;
				for each (nce in variables) {
					partialResult[4] &&= nce.evaluate();
				}
			}
			if (reputation.length > 0) {
				partialResult[5] = true;
				for each (nce in reputation) {
					partialResult[5] &&= nce.evaluate();
				}
			}
			if (knowledge.length > 0) {
				partialResult[6] = true;
				for each (nce in knowledge) {
					partialResult[6] &&= nce.evaluate();
				}
			}
			if (topicReputation.length > 0) {
				partialResult[7] = true;
				for each (nce in topicReputation) {
					partialResult[7] &&= nce.evaluate();
				}
			}
			if (time.length > 0) {
				partialResult[8] = true;
				for each (nce in time) {
					partialResult[8] &&= nce.evaluate();
				}
			}
			if (partialResult.length == 0) {
				return false;
			}
			var finalResult:Boolean = true;
			for each (var b:Boolean in partialResult) 
			{
				finalResult &&= b;
			}
			
			return finalResult;
		}
		
		
		
		/**
		 * Anyone else noticed how this method is totally retarded, and could be practically replaced by a better interface? Yeah. Exactly.
		 * TODO: unify skillDb/getStats/varDB/etc Dictionaries into one INumericPropertyProvider, INumericDictionaryProvider etc
		 */
		public function findTarget(scope:String, property:String, detail:String = null, parentDomain:IDomain = null):INumericValueProvider
		{
			var p:Player = ServiceLocator.getInstance().player;
			if (scope == TYPE_PLAYER_LAST_ACTION) {
				//bit is in this occasion an unique ident of the action
				if (!p.actionLog.lastTime.hasOwnProperty(property)) {
					//fuck if I know what to really do
					//trace ('HACK: returning last action time zero like a derp - ' + bit);
					return new ActionLogBit();
				}
				return p.actionLog.lastTime[property];
			}
			else if (scope == TYPE_PLAYER_ACTION_COUNT) {
				//bit is in this occasion an unique ident of the action
				return p.actionLog.getActionCount(property, detail);
			}
			else if (scope == TYPE_PLAYER_SKILL) {
				return p.skills.skillDb[property];
			}
			else if (scope == TYPE_PLAYER_STAT) {
				return p.stats.getStats()[property];
			}
			else if (scope == TYPE_PLAYER_VARIABLE) {
				return p.variables.varDb[property];
			}
			else if (scope == TYPE_PLAYER_KNOWLEDGE) {
				return p.knowledge.getStats()[property];
			}
			else if (scope == TYPE_TIME) {
				return GameTime.getCVE(property);
			}
			else if (scope == TYPE_CURRENT_ACTIVITY_PLAYER_REPUTATION) {
				if (parentDomain === null) {
					throw new Error ('Reputation requires a domain! Give me an Activity or something.');
				}
				return parentDomain.reputation.repDb[property];
			}
			else if (scope == TYPE_CURRENT_ACTIVITY_PLAYER_TOPIC_REPUTATION) {
				if (parentDomain === null) {
					throw new Error ('Reputation requires a domain! Give me an Activity or something.');
				}
				return parentDomain.reputation.repDb[property];
			}
			return null;
		}
		
		
		public function findAttribTarget(type:String, bit:String):IAttribProvider
		{
			//attrib section
			if (type == TYPE_ATTRIBUTE_PLAYER) {
				if (bit == '' || bit == 'player') {
					return ServiceLocator.getInstance().player;
				} //else get something else specific to player in the future
			}
			else if (type == TYPE_ATTRIBUTE_ACTIVITY) {
				return ServiceLocator.getInstance().player.activities.findActivity(bit);
			}
			return null;
		}
		/**
		 * =================================================================== handlers =======================================================================
		 */
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
		
		

		
	}
}
package uglycode.trollingberry.game
{
	public interface INumericValueProvider
	{
		//has a public value property
		function get value():Number;
		function set value(x:Number):void;
	}
}
package uglycode.trollingberry.world
{
	import flash.utils.Dictionary;
	
	public class Topics
	{
		public static const ART:String = 'art';
		public static const GAMING:String = 'gaming';
		public static const HEALTH:String = 'health';
		public static const HEALTH_CANCER:String = 'cancer';
		public static const DIET:String = 'diet';
		public static const DIET_VEGANISM:String = 'veganism';
		public static const GLOBALISATION:String = 'globalisation';
		public static const RANDOM_BLACKANDWHITE:String = 'world being black and white';
		
		public var topicDb:Dictionary = new Dictionary;
		
		public function Topics()
		{
			initTopics();
		}
		
		private function initTopics():void
		{
			topicDb[ART] = new Topic(ART, 'Knowledge of fine arts, with only a slight hint of pretentiousness.');
			topicDb[GAMING] = new Topic(GAMING, 'Familiarity with video games, gaming in general, and muscle atrophy.');
			topicDb[HEALTH] = new Topic(HEALTH, 'Interest in health, and a minor obsession with keeping it.');
			topicDb[HEALTH_CANCER] = new Topic(HEALTH_CANCER, 'Knowledge about the C-word. No, the other C-word.');
			topicDb[DIET] = new Topic(DIET, 'Knowing what to eat and how does it affect you. The utter depression that comes with the knowledge.');
			topicDb[DIET_VEGANISM] = new Topic(DIET_VEGANISM, "The proficiency in eating what's natural, and letting everyone else know about it, too.");
			topicDb[GLOBALISATION] = new Topic(GLOBALISATION, "Understanding the economics and the amount of slaves that enabled you to play this game. (Seventeen.)");
			topicDb[RANDOM_BLACKANDWHITE] = new Topic(RANDOM_BLACKANDWHITE, "As weird topic as any - theoretical discussion about how things are or aren't black and white, and all the shades of grey between, like #e3e3e3 and #888.");
			
		}
	}
}
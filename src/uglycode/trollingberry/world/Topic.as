package uglycode.trollingberry.world
{
	/**
	 * Topic model
	 */
	public class Topic
	{
		public var name:String;
		public var description:String;
		
		
		
		public function Topic(name:String, description:String = '')
		{
			this.name = name;
			this.description = description;
		}
	}
}
package uglycode.trollingberry.world.actions
{
	
	import uglycode.trollingberry.ServiceLocator;
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.game.INumericValueProvider;
	import uglycode.trollingberry.game.consequence.Condition;
	import uglycode.trollingberry.game.consequence.ConditionElement;
	import uglycode.trollingberry.game.consequence.Conditions;
	import uglycode.trollingberry.game.consequence.Effects;
	import uglycode.trollingberry.game.consequence.Outcome;
	import uglycode.trollingberry.world.activities.Activity;

	public class Action
	{
		public var conditions:Conditions = new Conditions();
		
		/**
		 * List all possible outcomes here
		 */
		public var outcomes:Vector.<Outcome> = new Vector.<Outcome>;
		/**
		 * if all outcomes are not elilgible, run this one without verification:
		 * FUTURE TODO: maybe change to a vector if it will prove useful? 
		 */
		public var defaultOutcome:Outcome = null;
		
		/**
		 * save which outcome was the last...
		 */
		public var lastOutcome:Outcome = null;
		
		/**
		 * if Conditions have not been met, [false] will hide this action from user, [true] will grey it out
		 * //ignored for now - need UI that supports Disabled elements
		 */
		private var visibleIfConditionsNotMet:Boolean = false;
		
		public var parentActivity:Activity;
		
		
		
		
		protected var _name:String = '';
		protected var _ident:String;
		[Deprecated]
		protected var _effects:Effects;
		private var _secretId:uint = 0;
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */

		
		public function Action(parent:Activity)
		{
			this.parentActivity = parent;
		}

		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		

		
		public function get ident():String {
			return _ident;
			//parentActivity.ident + '::' + this.ident; 
		}
		
		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
			var cleanup:RegExp = /[ .,=!?\s+]/gi;
			_ident = parentActivity.ident + '::' + value.toLowerCase().replace(cleanup, '-') + secretId;
		}
		
		
		public function get fullName():String
		{
			return '' + this.parentActivity.name + ': <b>' + this.name + '</b>';
		}
		
		public function get secretId():uint
		{
			return _secretId;
		}
		
		
		public function set secretId(value:uint):void
		{
			_secretId = value;
			name = _name;
		}

		/*
		public function get effects():Effects
		{
			return _effects;
		}
		*/
		public function get lastEffects():Effects {
			//todo if effects will vary from time to time, then store the last effects here - they'll be grabbed from here by Logging and/or View
			if (!lastOutcome) {
				return new Effects; //or null?
			}
			return lastOutcome.lastEffects;
		}
		

		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public function evaluate():Boolean {
			return this.conditions.evaluate();
		}
		
		
		
		public function execute(force:Boolean = false):Outcome {
			if (!evaluate() || force) {
				throw new Error('for some reason we are trying to run condition that does not pass validation.');
			}
			else if (force) {
				trace ('Forcing action! ' + this.ident);
			}
			/**
			 * TODO
			 * = maybe eval again
			 * = manage Outcomes:
			 * == order outcomes by priority
			 * == evaluate each
			 * == run the first by priority
			 * xxx if multiOutcome flag == true, run others with that flag // screw this for now //
			 * xxx merge their log messages / effects
			 */
			this.lastOutcome = null;
			var outcomesByPriority:Array = this.giveMeSortedOutcomes();
			for each (var vec:Vector.<Outcome> in outcomesByPriority) 
			{
				//trace ('processing outcomes with priority ' + vec[0].priority);
				for each (var o:Outcome in vec) 
				{
					if (o.evaluate()) {
						o.execute();
						this.lastOutcome = o;
						return this.lastOutcome;
					}
				}
			}
			/**
			 * Plan B:
			 * = if no Outcome evaluated, run the defaultOutcome without evaluation
			 * = if no defaultOutcome exists, run Player.runTimeSink()
			 */
			if (!lastOutcome && defaultOutcome) {
				trace ('running default Outcome for Action ' + this.ident);
				defaultOutcome.execute();
				this.lastOutcome = defaultOutcome;
				return this.lastOutcome;
			}
			else if (!lastOutcome && !defaultOutcome) {
				this.lastOutcome = ServiceLocator.getInstance().player.doTimeSinkAction();
				return this.lastOutcome;
			}
			var x:int = 0;
			return null;
		}

		/*
		public function addConditionFor(targetType:String, targetBit:String, method:String, value:Number):void {
		var c:Condition = new Condition;
		var target:IConditionable = c.findTarget(targetType, targetBit);
		c.???.push(new ConditionValueElement(target, method, value));
		this.conditions.conditions.push(c);
		}
		*/
		
		
		

		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		protected function addCondition(c:Condition):void {
			this.conditions.conditions.push(c);
		}
		
		
		
		private function giveMeSortedOutcomes():Array
		{
			var a:Array = [];
			for each (var o:Outcome in outcomes) {
				if (!a.hasOwnProperty(o.priority)) {
					a[o.priority] = new Vector.<Outcome>;
				}
				a[o.priority].push(o);
			}
			//a.sort(Array.NUMERIC);
			return a;
		}



		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
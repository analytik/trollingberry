package uglycode.trollingberry.world.life
{
	import uglycode.trollingberry.character.Reputation;
	import uglycode.trollingberry.game.IDomain;

	public class Person implements IDomain
	{
		protected var _name:String;
		protected var _reputation:Reputation;
		/**
		 * TODO FUTURE
		 * MASSIVE PLANS FOR WORLD DOMINATION etc
		 * 
		 * Move a bunch of props from Player to here (skills, stats, knowledge, variables // reputation, traits, disorders, relationships)
		 * ...and make them into IProvideHumanStats interface 
		 * 
		 * 
		 * 
		 * 
		 */
		
		public function Person()
		{
		}
		
		public function get name():String
		{
			return _name;
		}

		public function set name(value:String):void
		{
			_name = value;
		}

		public function get reputation():Reputation
		{
			return _reputation;
		}

		public function set reputation(value:Reputation):void
		{
			_reputation = value;
		}


	}
}
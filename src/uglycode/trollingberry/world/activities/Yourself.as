package uglycode.trollingberry.world.activities
{
	import uglycode.trollingberry.Player;
	import uglycode.trollingberry.character.skills.Skills;
	import uglycode.trollingberry.character.variables.Variables;
	import uglycode.trollingberry.game.ActionLog;
	import uglycode.trollingberry.game.consequence.Command;
	import uglycode.trollingberry.game.consequence.ConditionElement;
	import uglycode.trollingberry.game.consequence.Effects;
	import uglycode.trollingberry.game.consequence.Outcome;
	import uglycode.trollingberry.world.actions.Action;

	public class Yourself extends RealLifeActivity
	{
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */

		public function Yourself()
		{
			super();
			this.name = 'Yourself';
			this.description = "Well, yeah, this is your virtual real-life self. Confusing? You're the one playing a game about being on Internet, not me, I'm just making it. Not sure which one is worse.";
			//this._ident = 'tumblr';
			//initActions();
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		override public function initActions():void
		{
			trace ('Initing Yourself actions');
			//reset the bitch
			this._actions = new Vector.<Action>;

			// nap
			var shower:Action = new Action(this);
			shower.name = 'Take a shower';
			shower.conditions.addActionCountCondition(shower.ident, ConditionElement.FUNCTION_MAX, 1, ActionLog.SPAN_12H);
			shower.conditions.addVariableCondition(Variables.HYGIENE, ConditionElement.FUNCTION_MAX, 4);
			//a2.conditions.addSkillCondition(Skills.DRAWING, ConditionValueElement.FUNCTION_MAX, -1);
			var showerE:Effects = new Effects();
			showerE.immediate.push(Command.createValueCommand(Command.TYPE_VARIABLE, 'inc', Variables.HYGIENE, 14));
			shower.defaultOutcome = Outcome.create(shower, 'shower', "You took a shower. You've used way too much water, but still nowhere near as much as was consumed on the" +
				" phone you're playing this game on.", 25, null, showerE, 1, 5, 0.999, false, 
				"You failed to take a shower as the hot water was out for no good reason. Who would shower in a cold water? #firstworldproblems");
			this.actions.push(shower);
			
			// nap
			var a2:Action = new Action(this);
			a2.name = 'Try to nap for a while';
			a2.conditions.addActionCountCondition(a2.ident, ConditionElement.FUNCTION_MAX, 3, ActionLog.SPAN_24H);
			a2.conditions.addVariableCondition(Variables.EXHAUSTION, ConditionElement.FUNCTION_MIN, 8);
			a2.conditions.addVariableCondition(Variables.EXHAUSTION, ConditionElement.FUNCTION_MAX, 14);
			//a2.conditions.addSkillCondition(Skills.DRAWING, ConditionValueElement.FUNCTION_MAX, -1);
			var e2:Effects = new Effects();
			e2.immediate.push(Command.createValueCommand(Command.TYPE_VARIABLE, 'dec', Variables.EXHAUSTION, 1.9));
			a2.defaultOutcome = Outcome.create(a2, 'sleep-nap', "You get a refreshing 45-minute nap because I don't have time to program advanced simulation taking into account" +
				" your caffeine levels, ADHD, anxiety, brain activity, exhaustion, neuroses, light levels, bed quality, mosquito quantity, stomach ache and other bullshit.", 45, null, e2, 1, 7);
			this.actions.push(a2);
			
			// timed sleep
			var a1:Action = new Action(this);
			a1.name = 'Set an alarm and sleep 8hrs';
			a1.conditions.addActionCountCondition(a1.ident, ConditionElement.FUNCTION_EQUALS, 0, ActionLog.SPAN_20H);
			a1.conditions.addVariableCondition(Variables.EXHAUSTION, ConditionElement.FUNCTION_MIN, 14);
			a1.conditions.addVariableCondition(Variables.EXHAUSTION, ConditionElement.FUNCTION_MAX, 18);
			//a1.conditions.addSkillCondition(Skills.DRAWING, ConditionValueElement.FUNCTION_MAX, -1);
			var e1:Effects = new Effects();
			e1.immediate.push(Command.createValueCommand(Command.TYPE_VARIABLE, 'dec', Variables.EXHAUSTION, 19.5));
			a1.defaultOutcome = Outcome.create(a1, 'sleep-normal', "You set an alarm and manage to get exactly 7 hours, 50 minutes of good sleep, because I didn't yet complicate this " +
				"with all the memories of painful childhood that will keep you awake. Enjoy while it lasts.", 630, null, e1, 1, 7);
			this.actions.push(a1);
			
			// unlimited sleep
			var a0:Action = new Action(this);
			a0.name = 'Sleep like dead';
			a0.conditions.addActionCountCondition(a0.ident, ConditionElement.FUNCTION_EQUALS, 0, ActionLog.SPAN_24H);
			a0.conditions.addVariableCondition(Variables.EXHAUSTION, ConditionElement.FUNCTION_MIN, 18);
			//a0.conditions.addSkillCondition(Skills.DRAWING, ConditionValueElement.FUNCTION_MAX, -1);
			var e0:Effects = new Effects();
			e0.immediate.push(Command.createValueCommand(Command.TYPE_VARIABLE, 'dec', Variables.EXHAUSTION, 13.5));
			e0.immediate.push(Command.createValueCommand(Command.TYPE_VARIABLE, 'inc', Variables.SOMNOLENCE, 3.5));
			a0.defaultOutcome = Outcome.create(a0, 'sleep-long', "You were either too tired or too dumb to set an alarm, or maybe you just didn't care as you have no life anyway. " +
				"Either way, you've just slept damn too long, and you don't feel as refreshed as you would if you weren't a dumbass and managed your life better.", 845, null, e0, 1, 7);
			this.actions.push(a0);
			
			// mirror mirror
			var aa:Action = new Action(this);
			aa.name = 'Look in the mirror';
			var ea:Effects = new Effects();
			//e0.immediate.push(new Command(Command.TYPE_VARIABLE, 'dec', Variables.EXHAUSTION, 13.5));
			//e0.immediate.push(new Command(Command.TYPE_VARIABLE, 'inc', Variables.SOMNOLENCE, 3.5));
			aa.defaultOutcome = Outcome.create(aa, 'mirror-look', "If this game was finished, you'd see a list of your statistics, current mood, exhaustion, anxiety, adrenaline levels and similar." +
				" But good luck waiting for the final release.", 5, null, null, 1, 10);
			aa.outcomes.push(Outcome.create(aa, 'mirror-look1', "You looked in a mirror and came up with some melodramatic bullshit about parallels between real life and reflections and lakes" +
				" and puddles and suddenly you're thinking about puppies running around in a park. Time well spent.", 5, null, null, 0.15, 4));
			aa.outcomes.push(Outcome.create(aa, 'mirror-look2', "If this game was Dune 1, you'd see a face of Paul Atreides and his blue, blue eyes, and also have the option to Save and Load.", 5, null, null, 0.015, 3));
			aa.outcomes.push(Outcome.create(aa, 'mirror-look3', "You yawned and looked at the watch. Your exhaustion level is " + Math.floor(Player.getInstance().variables.varDb[Variables.EXHAUSTION].value) + '/20.', 5, null, null, 0.02, 2));
			this.actions.push(aa);
		}
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
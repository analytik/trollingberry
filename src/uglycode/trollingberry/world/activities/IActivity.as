package uglycode.trollingberry.world.activities
{
	import uglycode.trollingberry.world.actions.Action;

	//import starling.display.Image;

	public interface IActivity
	{
		//function get description():String;
		//function get logo():Image;
		function get isVisibleInMenu():Boolean;
		function get actions():Vector.<Action>;
		function get ident():String;
	}
}
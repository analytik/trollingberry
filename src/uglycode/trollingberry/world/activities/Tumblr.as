package uglycode.trollingberry.world.activities
{
	import uglycode.trollingberry.character.skills.Skills;
	import uglycode.trollingberry.character.variables.Variables;
	import uglycode.trollingberry.game.ActionLog;
	import uglycode.trollingberry.game.GenericNumberValueProvider;
	import uglycode.trollingberry.game.consequence.Command;
	import uglycode.trollingberry.game.consequence.Condition;
	import uglycode.trollingberry.game.consequence.ConditionElement;
	import uglycode.trollingberry.game.consequence.Effects;
	import uglycode.trollingberry.game.consequence.Outcome;
	import uglycode.trollingberry.world.actions.Action;

	public class Tumblr extends BlogCommunity implements IActivity
	{
		private var _regIdent:String;
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */

		public function Tumblr()
		{
			super();
			this.name = 'Tumblr';
			this.description = "It is exactly like watching a tumble drier - millions of images keep swirling in front of you, hypnotizing you. " +
				"You want to make some clothing-inspired art just by looking at it. But you're not. You're just letting it swirl.";
			//this._ident = 'tumblr';
			//initActions();
		}
		

		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		override public function initActions():void
		{
			trace ('Initing Tumblr actions');
			//reset the bitch
			this._actions = new Vector.<Action>;
			//TODO move this into OnlineActivity.addOnetimeRegistrationAction();
			/*
			<activity>
				<action name="Register">
					<conditions>
						<count source="register" method="==" value="0" />
					</conditions>
					<outcome default="true" ident="registration" priority="5" chance="1.0">
						<!-- condition ...-->
						<!-- effect -->
						<message failure="false">You've sucessf...</message>
					</outcome>
				</action>
				<action name="Confirm registration email">
					<conditions>
						<count source="register" method="==" vale="1" />
						<count source="confirm-registration-email" method="==" value="0" />
					</conditions>
					<outcome default="true" ident="conf-email" priority="5" chance="1.0">
						<!-- condition ...-->
						<!-- effect -->
						<message failure="false">After refreshing your inbox 5 times...</message>
					</outcome>
				</action>
				<action name="Doodle and then post it">
					<conditions>
						<count source="confirm-registration-email" method="==" value="1" />
					</conditions>
					<outcome default="true" ident="conf-email" priority="7" chance="1.0" time="25">
						<!-- condition ...-->
						<effects>
							<immediate>
								<effect target="skill" bit="drawing" action="inc" value="0.2" />
							</immediate>
						</effects>
						<message failure="false">You spent some time doodling something completely disgusting, but you posted it anyway.</message>
					</outcome>
				</action>
			</activity>
			*/

			//registration will available only if never registered before
			var a:Action = new Action(this);
			a.name = 'Register';
			a.conditions.addActionCountCondition(a.ident, ConditionElement.FUNCTION_EQUALS, 0);
			a.defaultOutcome = Outcome.create(a, 'registration', "You've successfully entered your email address. So proud of you.", 5);
			this.actions.push(a);
			
			//confirm email only if registration count == 1
			var a1:Action = new Action(this);
			a1.name = 'Confirm registration email';
			this._regIdent = a1.ident;
			// must have tried registering and must NOT have previous conf. email
			a1.conditions.addActionCountCondition(a.ident, ConditionElement.FUNCTION_EQUALS, 1);
			a1.conditions.addActionCountCondition(a1.ident, ConditionElement.FUNCTION_EQUALS, 0);
			a1.defaultOutcome = Outcome.create(a1, 'conf-email', "After refreshing your inbox 5 times, you managed to click the provided verification link. Still proud of you.", 5);
			this.actions.push(a1);
			
			var a0:Action = new Action(this);
			a0.name = 'Read TOS';
			a0.conditions.addActionCountCondition(a0.ident, ConditionElement.FUNCTION_EQUALS, 0);
			a0.defaultOutcome = Outcome.create(a0, 'read-tos', "Yes, so you managed to find out that you shouldn't post racist comments, hate on other people, post a lot of p0rn videos, " +
				"and also that you absolutely need to be over 13, and if you're not, you should ask your parents to buy you an XBOX instead. Been there, done that.", 45);
			this.actions.push(a0);
			
			var a2:Action = new Action(this);
			a2.name = 'Doodle and then post it';
			addRegCondition(a2);
			a2.conditions.addActionCountCondition(a2.ident, ConditionElement.FUNCTION_MAX, 7, ActionLog.SPAN_7DAYS);
			a2.conditions.addSkillCondition(Skills.DRAWING, ConditionElement.FUNCTION_MAX, -1);
			var e2:Effects = new Effects();
			e2.immediate.push(Command.createValueCommand('skill', 'inc', 'drawing', 0.3));
			a2.defaultOutcome = Outcome.create(a2, 'draw-doodle1', "You spent some time doodling something completely disgusting, but you posted it anyway.", 25, null, e2, 1, 7);
			this.actions.push(a2);
			
			var a3:Action = new Action(this);
			a3.name = 'Doodle and then post it';
			a3.secretId = 3;
			a3.conditions.addActionCountCondition(a1.ident, ConditionElement.FUNCTION_EQUALS, 1);
			a3.conditions.addActionCountCondition(a2.ident, ConditionElement.FUNCTION_MIN, 8, ActionLog.SPAN_7DAYS);
			a3.conditions.addSkillCondition(Skills.DRAWING, ConditionElement.FUNCTION_MAX, -1);
			var e3:Effects = new Effects();
			e3.immediate.push(Command.createValueCommand('skill', 'inc', 'drawing', 0.1));
			a3.defaultOutcome = Outcome.create(a3, 'draw-doodle2', "You spent some time doodling something disgusting, but you posted it anyway.", 30, null, e3, 1, 6);
			this.actions.push(a3);
			
			var a4:Action = new Action(this);
			a4.name = 'Look at the Explore thingy';
			a4.conditions.addActionCountCondition(a4.ident, ConditionElement.FUNCTION_MAX, 4, ActionLog.SPAN_24H);
			a4.defaultOutcome = Outcome.create(a4, 'explore-long', "You've managed to spend a full hour staring at random promoted content, and feeling like you should do something great with your life, too.", 60);
			this.actions.push(a4);
			
			var chain1_01:Action = new Action(this);
			chain1_01.name = 'Post babe pictures';
			chain1_01.secretId = 0x010101; //01 - tumblr, 01 - chain #1, 01 - part 1
			addRegCondition(chain1_01);
			chain1_01.conditions.addVariableCondition(Variables.SEXUAL_FRUSTRATION, ConditionElement.FUNCTION_MIN, -18); //TODO adjust before production to +3 maybe
			chain1_01.conditions.addActionCountCondition(chain1_01.ident, ConditionElement.FUNCTION_EQUALS, 0, ActionLog.SPAN_14DAYS, 5);
			var eChain1_01:Effects = new Effects();
			eChain1_01.immediate.push(Command.createAttribCommand(Command.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', 1));
			chain1_01.defaultOutcome = Outcome.create(chain1_01, 'post babe pix', "After browsing and reblogging some scantily clad babes (which mildly increased your lust), you have 1 new message in your inbox. ", 30, null, eChain1_01, 1, 6);
			this.actions.push(chain1_01);
			
			var chain1_02:Action = new Action(this);
			chain1_02.name = 'Read an inbox reaction to babe pix';
			chain1_02.secretId = 0x010102; //01 - tumblr, 01 - chain #1, 01 - part 1
			addRegCondition(chain1_02);
			chain1_02.conditions.addNumericAttribCondition(Condition.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', ConditionElement.FUNCTION_EQUALS, 1);
			var eChain1_02:Effects = new Effects();
			eChain1_02.immediate.push(Command.createAttribCommand(Command.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', 2));
			chain1_02.defaultOutcome = Outcome.create(chain1_02, 'read babe pix inbox', "It seems that you have a brave anonymous message from one of those so-called Social Justice Warriors." +
				" This one is quite predictable, saying how you objectify women and how men are scum. You're not quite sure how to react.", 5, null, eChain1_02, 1, 6);
			this.actions.push(chain1_02);
			
			// branch - ignoring (ends here)
			
			var chain1_03a:Action = new Action(this);
			chain1_03a.name = 'Ignore the SWJ inbox message';
			chain1_03a.secretId = 0x010103; //01 - tumblr, 01 - chain #1, 01 - part 1
			addRegCondition(chain1_03a);
			chain1_03a.conditions.addNumericAttribCondition(Condition.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', ConditionElement.FUNCTION_EQUALS, 2);
			var eChain1_03a:Effects = new Effects();
			eChain1_03a.immediate.push(Command.createAttribCommand(Command.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', 50));
			chain1_03a.defaultOutcome = Outcome.create(chain1_03a, 'ignore babe pix inbox', "You did the only smart thing and just plain ignored the message. Now you're bored, though.", 5, null, 
				eChain1_03a, 1, 6);
			this.actions.push(chain1_03a);
			
			// branch - peaceful (doesn't last long)
			
			var chain1_03b:Action = new Action(this);
			chain1_03b.name = 'Engange the SWJ with a peaceful reaction';
			chain1_03b.secretId = 0x010104; //01 - tumblr, 01 - chain #1, 01 - part 1
			addRegCondition(chain1_03b);
			chain1_03b.conditions.addNumericAttribCondition(Condition.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', ConditionElement.FUNCTION_EQUALS, 2);
			var eChain1_03b:Effects = new Effects();
			eChain1_03b.immediate.push(Command.createAttribCommand(Command.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', 20));
			chain1_03b.defaultOutcome = Outcome.create(chain1_03b, 'peaceful babe pix inbox1', "You did your best in trying to reply politely to the SWJ person.", 5, null, 
				eChain1_03b, 1, 6);
			this.actions.push(chain1_03b);
			
			// branch - ignoring (ends here)
			
			var chain1_03c:Action = new Action(this);
			chain1_03c.name = 'Respond to the SWJ angrily';
			chain1_03c.secretId = 0x010105; //01 - tumblr, 01 - chain #1, 01 - part 1
			addRegCondition(chain1_03c);
			chain1_03c.conditions.addNumericAttribCondition(Condition.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', ConditionElement.FUNCTION_EQUALS, 2);
			var eChain1_03c:Effects = new Effects();
			eChain1_03c.immediate.push(Command.createAttribCommand(Command.TYPE_ATTRIBUTE_ACTIVITY, this.ident, 'arc_01_stage', 20));
			chain1_03c.defaultOutcome = Outcome.create(chain1_03c, 'angry babe pix inbox1', "You decided to engage the SWJ in a fearceful, elaborate argument about what" +
				" you believe is your right.", 30, null, 
				eChain1_03c, 1, 6);
			this.actions.push(chain1_03c);
			
		}
		
		

		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */

		
		private function addRegCondition(act:Action):void
		{
			act.conditions.addActionCountCondition(_regIdent, ConditionElement.FUNCTION_EQUALS, 1);
		}		
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

		
	}
}
package uglycode.trollingberry.world.activities
{
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.game.IStatProvider;
	import uglycode.trollingberry.world.actions.Action;
	
	public class Activities implements IStatProvider
	{
		
		private var actDb:Dictionary = new Dictionary();
		
		public static var YOURSELF:String = 'Your needs';
		public static var TIMESINK:String = 'Wasting time';
		public static var TUMBLR:String = 'Tumble drier';
		public static var PLANETCRAP:String = 'Planetpoop';
		public static var LIVEJOURNAL:String = 'WhineJournal';
		public static var FOURCHAN:String = 'Raw Sewage';
		public static var FACEBOOK:String = 'Fecesbook';
		public static var TECHCRUNCH:String = 'Gadgeteria';
		public static var QT3:String = '3QP2';
		public static var WOW:String = 'ClickCraft';
		//v2: torrenting, digg, reddit, porn
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		public function Activities()
		{
			//initActivities();
		}

		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		public function getVisibleActivities():Vector.<Activity>
		{
			var v:Vector.<Activity> = new Vector.<Activity>;
			for each (var a:Activity in actDb) 
			{
				if (a.isVisibleInMenu === true) {
					v.push(a);
				}
			}
			return v;
		}
		
		
		public function getVerbalType():String {
			return 'activity';
		}
		public function getStats():Dictionary {
			return actDb;
		}
		public function isMehSkillDescriptionVisible():Boolean {
			return false;
		}
		public function isGaugeDisplayed():Boolean {
			return true;
		}
		public function getStatVisibilityMargin():Number {
			return -20.0;
		}
		public function getStatVisibilityDeadzone():Number {
			return 3;
		}
		public function getStatTitle():String {
			return 'Your stats, like in an RPG:';
		}
		
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		public function initActivities():void
		{
			actDb[YOURSELF] = new Yourself();
			actDb[YOURSELF].initActions();
			actDb[TIMESINK] = new TimeSink();
			actDb[TUMBLR] = new Tumblr();
			actDb[TUMBLR].initActions();
			actDb[PLANETCRAP] = new PlanetCrap();
			//actDb[LIVEJOURNAL] = new LiveJournal();
			//actDb[FOURCHAN] = new FourChan();
			//actDb[FACEBOOK] = new Facebook();
			//actDb[TECHCRUNCH] = new Techcrunch();
			//actDb[QT3] = new QT3();
			actDb[WOW] = new WorldOfClickcraft();
		}
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		
		
		public function findAction(ident:String):Action {
			for each (var act:IActivity in actDb)
			{
				for each (var a:Action in act.actions)
				{
					if (a.name == ident) {
						return a;
					}
				}
			}
			throw new Error ('Action not found anywhere, sorry! Fix it, umkay?');
		}
		
		
		
		public function findActivity(ident:String):Activity
		{
			for each (var act:Activity in actDb)
			{
				if (act.ident == ident) {
					return act;
				}
			}
			return null;
		}
		
		
		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

		private function tempRandomizeStats():void {
			for (var s:String in actDb) {
				actDb[s].value = (Math.random()*40) - 20;
			}
		}
		
		
	}
}
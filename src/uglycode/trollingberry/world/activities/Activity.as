package uglycode.trollingberry.world.activities
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.utils.Dictionary;
	
	import uglycode.trollingberry.character.Reputation;
	import uglycode.trollingberry.game.IAttribProvider;
	import uglycode.trollingberry.game.IDomain;
	import uglycode.trollingberry.world.actions.Action;

	public class Activity implements IActivity, IDomain, IAttribProvider
	{
		protected var isInitiallyHidden:Boolean = false;
		protected var isDiscovered:Boolean = true;
		protected var _isVisibleInMenu:Boolean = true;
		protected var _reputation:Reputation = new Reputation(this);
		//future public var memberCount:uint = 50;
		protected var _attribsString:Dictionary = new Dictionary();
		protected var _attribsNumeric:Dictionary = new Dictionary();
		protected var _attribsComplex:Dictionary = new Dictionary();
		
		protected var _actions:Vector.<Action> = new Vector.<Action>;
		
		protected var _name:String = 'Activity';
		protected var _ident:String = '__overwrite_this__';
		public var description:String;
		public var icon:Bitmap = new Bitmap(new BitmapData(128,128,false,0));
		
		

		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		public function Activity()
		{
			icon.bitmapData.perlinNoise(128,128,1+Math.random()*31,(65000 * Math.random()),false,false,9 + (Math.random()*6),false);
		}
		
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		
		public function get actions():Vector.<Action> {
			return _actions;
		}
		
		public function initActions():void {
			throw new Error('Override me, fuckass.');
		}
		
		
		public function get isVisibleInMenu():Boolean
		{
			return _isVisibleInMenu;
		}
		
		public function set isVisibleInMenu(value:Boolean):void
		{
			_isVisibleInMenu = value;
		}
		
		public function get ident():String {
			return _ident;
		}
		
		public function get name():String
		{
			return _name;
		}
		
		public function set name(value:String):void
		{
			_name = value;
			var cleanup:RegExp = /[ .,=!?\s+]/gi;
			_ident = value.toLowerCase().replace(cleanup, '-');
		}
		
		public function get reputation():Reputation
		{
			return _reputation;
		}
		
		public function set reputation(value:Reputation):void
		{
			_reputation = value;
		}
		
		/**
		 * IAttribProvider methods
		 */
		public function hasAttribIndex(key:String):Boolean {
			return _attribsString.hasOwnProperty(key) || _attribsNumeric.hasOwnProperty(key) || _attribsComplex.hasOwnProperty(key);
		}
		public function hasStringAttribIndex(key:String):Boolean {
			return _attribsString.hasOwnProperty(key);
		}
		public function hasNumericAttribIndex(key:String):Boolean {
			return _attribsNumeric.hasOwnProperty(key);
		}
		public function hasComplexAttribIndex(key:String):Boolean {
			return _attribsComplex.hasOwnProperty(key);
		}
		
		public function setStringAttrib(key:String, value:String):void {this._attribsString[key] = value;}
		public function setNumericAttrib(key:String, value:Number):void {this._attribsNumeric[key] = value;}
		public function setComplexAttrib(key:String, value:Object):void {this._attribsComplex[key] = value;}
		

		public function getStringAttrib(key:String):String {
			return hasStringAttribIndex(key) ? _attribsString[key] : null;
		}
		public function getNumericAttrib(key:String):Number {
			return hasNumericAttribIndex(key) ? _attribsNumeric[key] : null;
		}
		public function getComplexAttrib(key:String):Object {
			return hasComplexAttribIndex(key) ? _attribsComplex[key] : null;
		}
		
		public function get allStringAttribs():Dictionary {
			return this._attribsString;
		}
		public function get allNumericAttribs():Dictionary {
			return this._attribsNumeric;
		}
		public function get allComplexAttribs():Dictionary {
			return this._attribsComplex;
		}
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		public function getAvailableActions():Vector.<Action>
		{
			var filteredActions:Vector.<Action> = new Vector.<Action>;
			for each (var a:Action in actions) 
			{
				if (a.evaluate()) {
					filteredActions.push(a);
				}
			}
			
			return filteredActions;
		}

		
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */

	}
}
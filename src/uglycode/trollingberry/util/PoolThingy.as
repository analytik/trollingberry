package uglycode.trollingberry.util
{
	import flash.errors.IllegalOperationError;
	
	import qnx.fuse.ui.actionbar.ActionBar;
	import qnx.fuse.ui.text.TextAlign;
	import qnx.fuse.ui.text.TextFormat;
	
	import uglycode.trollingberry.ServiceLocator;

	public class PoolThingy implements IService
	{
		private static var _instance:PoolThingy;
		private static var formatDefault:TextFormat;
		private static var formatTitle:TextFormat;
		private static var formatLink:TextFormat;
		private static var formatSecondary:TextFormat;
		private static var formatSmall:TextFormat;
		//private static var defaultFont:String = 'Slate Pro'; //live
		private static var defaultFont:String = 'Myriad Pro'; //development
		private static var formatDesc:TextFormat;
		private static var formatTiny:TextFormat;
		private static var formatSmallTime:TextFormat;
		private static var formatSmallTitle:TextFormat;
		
		
		public function PoolThingy()
		{
			if (_instance) {
				throw new IllegalOperationError("Singleton already constructed!");
			}
			_instance = this;
		}
		
		
		
		/*
		public static function getOldTextFormat(type:String = 'default'):flash.text.TextFormat {
			var tf:flash.text.TextFormat = new flash.text.TextFormat();
			if (type == 'desc') {
				tf.font = defaultFont;
				tf.size = 24;
				tf.align = TextFormatAlign.JUSTIFY;
				tf.color = 0xb1b1b1;
			}
			else if (type == 'default') {
				tf.font = defaultFont;
				tf.size = 45;
				tf.align = TextFormatAlign.LEFT;
				tf.color = 0xfafafa;
			}
			return tf;
		}
		*/
		
		
		public static function getTextFormat(type:String = 'default'):TextFormat {
			/**
			 * Official font: Slate Pro Regular / Light / Bold
			 * Official colors: primary - #fafafa, secondary - b1b1b1, link #00a8df
			 * Official sizes: 24 30 35 40 45 48 59 79
			 */
			if (type == 'default') {
				if (formatDefault === null) {
					formatDefault = new TextFormat();
					formatDefault.font = defaultFont;
					formatDefault.color = 0xfafafa;
					formatDefault.size = 45;
					formatDefault.align = TextAlign.LEFT;
				}
				return formatDefault;
			}
			if (type == 'small') {
				if (formatSmall === null) {
					formatSmall = new TextFormat();
					formatSmall.font = defaultFont;
					formatSmall.color = 0xfafafa;
					formatSmall.size = 30;
					formatSmall.align = TextAlign.CENTER;
				}
				return formatSmall;
			}
			if (type == 'smalltitle') {
				if (formatSmallTitle === null) {
					formatSmallTitle = new TextFormat();
					formatSmallTitle.font = defaultFont;
					formatSmallTitle.color = 0xfafafa;
					//formatSmallTitle.bold = true;
					formatSmallTitle.size = 30;
					formatSmallTitle.align = TextAlign.CENTER;
				}
				return formatSmallTitle;
			}
			if (type == 'smalltime') {
				if (formatSmallTime === null) {
					formatSmallTime = new TextFormat();
					formatSmallTime.font = defaultFont;
					formatSmallTime.color = 0xb1b1b1;
					formatSmallTime.size = 30;
					formatSmallTime.align = TextAlign.RIGHT;
				}
				return formatSmallTime;
			}
			if (type == 'verysmall') {
				if (formatTiny === null) {
					formatTiny = new TextFormat();
					formatTiny.font = defaultFont;
					formatTiny.color = 0xfafafa;
					formatTiny.size = 24
					formatTiny.align = TextAlign.CENTER;
				}
				return formatTiny;
			}
			if (type == 'desc') {
				if (formatDesc === null) {
					formatDesc = new TextFormat();
					formatDesc.font = defaultFont;
					formatDesc.color = 0xb1b1b1;
					formatDesc.size = 30;
					formatDesc.align = TextAlign.JUSTIFY;
				}
				return formatDesc;
			}
			if (type == 'title') {
				if (formatTitle === null) {
					formatTitle = new TextFormat();
					formatTitle.font = defaultFont;
					formatTitle.color = 0xfafafa;
					formatTitle.size = 59; 
					formatTitle.align = TextAlign.CENTER;
				}
				return formatTitle;
			}
			if (type == 'link') {
				if (formatLink === null) {
					formatLink = new TextFormat();
					formatLink.font = defaultFont;
					formatLink.color = 0xa8df;
					formatLink.size = 45;
					formatLink.align = TextAlign.LEFT;
				}
				return formatLink;
			}
			if (type == 'secondary') {
				if (formatSecondary === null) {
					formatSecondary = new TextFormat();
					formatSecondary.font = defaultFont;
					formatSecondary.color = 0xb1b1b1;
					formatSecondary.size = 40;
					formatSecondary.align = TextAlign.LEFT;
				}
				return formatLink;
			}
			return formatDefault;
		}
		
		
		
		public static function getActionBar():ActionBar {
			var ab:ActionBar = new ActionBar();
			//ab.minHeight = 140; //deprecated as of BB10 GOLD SDK
			ab.setPosition(0,1140);
			/*
			var ta1:TabAction = new TabAction('stats');
			ta1.icon = new BitmapData(128,128,false, 0x0022ee);
			ab.addAction(ta1);
			var ta2:TabAction = new TabAction('log');
			ta2.icon = new BitmapData(128,128,false, 0x00ee22);
			ab.addAction(ta2);
			var ta3:TabAction = new TabAction('options');
			ta3.icon = new BitmapData(128,128,false, 0x661199);
			ab.addAction(ta3);
			//this.addChild(ab);
			*/
			return ab;
		}
		
		
		
		public static function getEmptyActionBar():ActionBar {
			var ab:ActionBar = new ActionBar();
			//ab.minHeight = 140; //deprecated as of BB10 GOLD SDK
			ab.setPosition(0,1140);
			return ab;
		}
		
		
		
		public function getPattern():String {
			return 'static';
		}
		public static function getInstance():PoolThingy {
			if (_instance) {
				return _instance;
			}
			return _instance;
		}
		public static function getNewInstance():PoolThingy {
			return PoolThingy.getInstance();
		}
	}
}
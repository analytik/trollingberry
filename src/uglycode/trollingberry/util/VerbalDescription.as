package uglycode.trollingberry.util
{
	import uglycode.trollingberry.character.variables.Variables;

	public final class VerbalDescription
	{
		public static const TOPIC_KNOWLEDGE_STRING:Array = [
			[
				"are clueless about",
				'get confused on the topic of', //too verbose?
				'want to get better in',
				'are an expert on'
			],
			[
				"are a premium retard when it comes to",
				'have problems understanding',
				'can talk about',
				'are a known guru of'
			],
			[
				"know nothing about",
				'know little about',
				'know stuff about',
				'know everything about'
			]
		];
		
		public function VerbalDescription()
		{
		}
		
		public static function ofSkill(skill:Number, ignoreMeh:Boolean = true):String {
			if (skill < -15) {
				return "awful";
			}
			else if (skill < -10) { return "bad" }
			else if (skill < -3) { return "sucky" }
			else if (skill > 17) { return "badass" }
			else if (skill > 12) { return "pro" }
			else if (skill > 8) { return "good" }
			else if (skill > 3) { return "okay" }
			if (ignoreMeh) return '';
			return "meh";
		}
		
		
		
		public static function getRandomKnowledgeLevels():Array
		{
			return (Math.random() < 0.33) ? VerbalDescription.TOPIC_KNOWLEDGE_STRING[0] :
				(Math.random() < 0.33) ? VerbalDescription.TOPIC_KNOWLEDGE_STRING[1] :
				VerbalDescription.TOPIC_KNOWLEDGE_STRING[2];
		}
		
		
		
		[Deprecated]
		public static function ofVariable(name:String, value:Number):String {

			/*
			varDb[BODY_ACTIVITY_INDEX] = new Variable(BODY_ACTIVITY_INDEX); //-20 = BAI 1.0 (lying down), 0 = 2.0, +20 = 3.0
			varDb[SEXUAL_FRUSTRATION] = new Variable(SEXUAL_FRUSTRATION);	//-20 = asexual, 0 = content, +20 = super horny
			varDb[PEER_PRESSURE] = new Variable(PEER_PRESSURE);				//-20 = independent, 0 = content, +20 = super insecure
			varDb[SLEEP_DEPRIVATION] = new Variable(SLEEP_DEPRIVATION);		//-20 = 10hrs sleep, -10 = 7.5hr sleep a day, 0 = +20 = less than 0.
			varDb[SOMNOLENCE] = new Variable(SOMNOLENCE);					//-20 = cool, +20 = fucking zombie
			varDb[EXHAUSTION] = new Variable(EXHAUSTION);					//-20 = full of energy, 0 = normal, +20 = dead tired
			varDb[HYGIENE] = new Variable(HYGIENE, 15);						//-20 = hobo, 0 = needs shower, 15 = clean, 20 = squeaky clean
			// LONG TERM TODO: MBTI affect these numbers
			varDb[CREATIVITY] = new Variable(CREATIVITY, 0, 1, -1, true, 120, 7);	//-20 = lame, +20 = super cool ideas
			varDb[WILLPOWER] = new Variable(WILLPOWER, 0, 1, -1, true, 90, 5);		//-20 = not doing shit, +20 - super active
			varDb[SOCIAL_INCLINATION] = new Variable(SOCIAL_INCLINATION, 0, 1, -1, true, 150, 7);		//-20 = asocial, +20 = needs partying now
			varDb[PHYSICAL_INCLINATION] = new Variable(PHYSICAL_INCLINATION, 0, 1, -1, true, 180, 7);	//-20 = sloth, +20 = needs running around
			*/
			if (name == Variables.BODY_ACTIVITY_INDEX) {}
			else if (name == Variables.SEXUAL_FRUSTRATION) {}
			else if (name == Variables.PEER_PRESSURE) {}
			else if (name == Variables.SLEEP_DEPRIVATION) {}
			else if (name == Variables.SOMNOLENCE) {}
			else if (name == Variables.EXHAUSTION) {}
			else if (name == Variables.HYGIENE) {}
			return '';
		}
	}
}
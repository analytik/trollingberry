package uglycode.trollingberry
{
	import flash.utils.Dictionary;
	import flash.utils.getTimer;
	
	import uglycode.trollingberry.character.TopicKnowledge;
	import uglycode.trollingberry.character.skills.Skills;
	import uglycode.trollingberry.character.stats.Stats;
	import uglycode.trollingberry.character.variables.Variables;
	import uglycode.trollingberry.game.ActionLog;
	import uglycode.trollingberry.game.GameTime;
	import uglycode.trollingberry.game.IAttribProvider;
	import uglycode.trollingberry.game.IHuman;
	import uglycode.trollingberry.game.IStatProvider;
	import uglycode.trollingberry.game.ITimeAdvancable;
	import uglycode.trollingberry.game.consequence.Outcome;
	import uglycode.trollingberry.util.IService;
	import uglycode.trollingberry.world.actions.Action;
	import uglycode.trollingberry.world.activities.Activities;
	import uglycode.trollingberry.world.activities.Activity;
	import uglycode.trollingberry.world.activities.TimeSink;

	/**
	 * A model class representing a all player data (so it can be serialized to a save files easily)
	 */
	public class Player implements IService, IHuman, IAttribProvider, ITimeAdvancable
	{
		/**
		 * data properties
		 */
		public var skills:Skills;
		public var knowledge:TopicKnowledge;
		public var stats:Stats;
		public var variables:Variables;
		//reputation is be stored per Domain
		public var actionLog:ActionLog;
		
		public var activities:Activities;
		protected var _lastActivity:Activity;
		protected var _currentActivity:Activity;
		[Deprecated]
		private var _availableActions:Object;
		[Deprecated]
		private var _availableActivities:Object;
		
		// LONGTERM V2
		//public var disorders:Object;
		//public var traits:Object;
		//public var friends:Object;
		//public var previousEvents:Object;
		//public var items:Object;
		//public var lifeRoles:Object;
		//public var projects:Object;
		
		/**
		 * own properties
		 */
		public var name:String = 'Player One';
		public var creationDate:int;		//unix timestamp [s]
		public var mostRecentGameStart:int	//getTimer()
		public var totalGameTime:int;		//at Save&Quit, save & add getTimer() [s]
		public var isPlayer:Boolean = true; //lol useless?
		private static var _instance:Player;
		
		private var _attribsString:Dictionary = new Dictionary();
		private var _attribsNumeric:Dictionary = new Dictionary();
		private var _attribsComplex:Dictionary = new Dictionary();
		
		
		
		
		/**
		 * =================================================================== CONSTRUCTOR, duh ===============================================================
		 */
		
		
		
		public function Player()
		{
			var tempDate:Date = new Date();
			creationDate = (tempDate.getTime() / 1000) as int;
			mostRecentGameStart = getTimer();
			//reputation = new Reputation();
			GameTime.registerForTimePassage(this);
		}
		
		
		/**
		 * =================================================================== getters / setters ==============================================================
		 */
		
		public function getPattern():String {
			return 'singleton';
		}
		
		
		
		public static function getInstance():Player {
			if (_instance) {
				return _instance;
			}
			_instance = new Player();
			return _instance;
		}
		
		
		
		public function set currentActivity(a:Activity):void {
			this._lastActivity = this._currentActivity;
			this._currentActivity = a;
		}
	
		public function get currentActivity():Activity {
			return this._currentActivity;
		}

		
		
		public function getVisibleActivities():Vector.<Activity> {
			return activities.getVisibleActivities().sort(sortActivityByName);
		}
		
		public function hasAttribIndex(key:String):Boolean {
			return _attribsString.hasOwnProperty(key) || _attribsNumeric.hasOwnProperty(key) || _attribsComplex.hasOwnProperty(key);
		}
		public function hasStringAttribIndex(key:String):Boolean {
			return _attribsString.hasOwnProperty(key);
		}
		public function hasNumericAttribIndex(key:String):Boolean {
			return _attribsNumeric.hasOwnProperty(key);
		}
		public function hasComplexAttribIndex(key:String):Boolean {
			return _attribsComplex.hasOwnProperty(key);
		}
		
		public function setStringAttrib(key:String, value:String):void {this._attribsString[key] = value;}
		public function setNumericAttrib(key:String, value:Number):void {this._attribsNumeric[key] = value;}
		public function setComplexAttrib(key:String, value:Object):void {this._attribsComplex[key] = value;}
		

		public function getStringAttrib(key:String):String {
			return hasStringAttribIndex(key) ? _attribsString[key] : null;
		}
		public function getNumericAttrib(key:String):Number {
			return hasNumericAttribIndex(key) ? _attribsNumeric[key] : null;
		}
		public function getComplexAttrib(key:String):Object {
			return hasComplexAttribIndex(key) ? _attribsComplex[key] : null;
		}
		
		public function get allStringAttribs():Dictionary {
			return this._attribsString;
		}
		public function get allNumericAttribs():Dictionary {
			return this._attribsNumeric;
		}
		public function get allComplexAttribs():Dictionary {
			return this._attribsComplex;
		}
		
		
		/////////////////////// ITimeAdvancable
		public function advanceTime(minutes:Number):void {
			if (this.variables) {
				variables.advanceTime(minutes);
			}
			else {
				trace ('Tried to advance time for Player.variables, but they don"t exist yet!');
			}
		}
		
		/**
		 * =================================================================== init ===========================================================================
		 */
		
		
		
		/**
		 * =================================================================== public methods =================================================================
		 */
		
		public function newGame():void {
			trace('starting new game');
			stats = new Stats();
			variables = new Variables();
			skills = new Skills();
			knowledge = new TopicKnowledge(this);
			actionLog = new ActionLog();
			this.activities = new Activities();
			this.activities.initActivities();
			GameTime.reset();
			//todo replace with forcing Action on player that plays intro / start
			GameTime.advance(600); //cannot start at midnight, innit
		}
		
		
		
		public function provideStats():Vector.<IStatProvider> {
			var v:Vector.<IStatProvider> = new Vector.<IStatProvider>;
			v.push(skills, stats, knowledge, variables);
			return v;
		}
		
		
		
		public function doAction(a:Action, force:Boolean = false):Outcome {
			//TODO Automation Subsystem 
			/*
			if (!force) {
				var a2:Action = PlayerAutomation.getForcedAutomatedAction();
				if a2 is not null, a = a2;
			}
			*/
			if (a.evaluate() || force) {
				//trace ('Player.doAction: running and logging action ' + a.ident);
				var o:Outcome = a.execute(force);
				this.actionLog.logOutcome(o);
				trace ('time now: Day ' + GameTime.day + ', ' + GameTime.hour + ':' + GameTime.minute + ' (' + GameTime.time + ')' );
				//trace ('drawing skill ' + skills.skillDb['drawing'].value);
				return o;
			}
			return null;
		}
		
		
		
		
		public function doTimeSinkAction():Outcome
		{
			// first, find time sink activity
			var activ:Activity = this.activities.findActivity('time-sink');
			// FUTURE TODO - if last action was online, then online time sink; vice versa
			// second, getTimeSinkAction()
			var action:Action = (activ as TimeSink).findInclinedAction();
			// third, execute()
			return this.doAction(action);
		}
		/**
		 * =================================================================== handlers =======================================================================
		 */
		
		
		
		/**
		 * ====================================================== game logic / other protected / private methods ==============================================
		 */
		
		private function sortActivityByName(x:Activity, y:Activity):Number {
			if (x.name == y.name) {
				return 0;
			}
			if (x.name > y.name) {
				return 1;
			}
			return -1;
		}
		
		/**
		 * ====================================================== TEMPORARY BULLSHIT - TODO EVERYTHING BELOW ==================================================
		 */
		
		
		
		
		
		
		
		
		
		
		
		
		
		

	}
}